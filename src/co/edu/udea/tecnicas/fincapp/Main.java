package co.edu.udea.tecnicas.fincapp;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("view/vista-principal.fxml"));
        primaryStage.setTitle("Fincapp");
        primaryStage.setScene(new Scene(root, 700, 500));
        primaryStage.show();
    }
 //comentario de prueba, prueba 2
// comentario prueba 3
    public static void main(String[] args) {
        launch(args);
    }
}
