package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.RegistroGeneralYaExisteException;
import co.edu.udea.tecnicas.fincapp.dao.GrupoAnimalDAO;
import co.edu.udea.tecnicas.fincapp.dao.MarranoDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.GrupoAnimalDAONIO;
import co.edu.udea.tecnicas.fincapp.dao.impl.MarranoDAONIO;
import co.edu.udea.tecnicas.fincapp.model.GrupoAnimal;

import java.util.List;
import java.util.Optional;

// todo hacer las validaciones del negocio
public class GrupoAnimalBsn {

    private GrupoAnimalDAO grupoAnimalDAO;


    public  GrupoAnimalBsn(){
        this.grupoAnimalDAO = new GrupoAnimalDAONIO();


    }

    public void registrarGrupoAnimal(GrupoAnimal grupoAnimal) throws RegistroGeneralYaExisteException {
        Optional<GrupoAnimal> grupoAnimalOptional = this.grupoAnimalDAO.consultarPorCodigo(grupoAnimal.getCodigo());
        // el registro no estaba
        if (grupoAnimalOptional.isPresent()) {
            throw new RegistroGeneralYaExisteException();
        } else {
            this.grupoAnimalDAO.registrarGrupoAnimal(grupoAnimal);
        }

    }

    public List<GrupoAnimal> mostrarGrupoAnimal(){
        return grupoAnimalDAO.mostrarGrupoAnimal();
    }


    public  String codigoGeneral(){
        return this.grupoAnimalDAO.codigoGeneral();
    }
    public String dietaGeneral(){
        return this.grupoAnimalDAO.dietaGeneral();
    }
    public int cantidadGeneral() {
        return this.grupoAnimalDAO.cantidadGeneral();
    }
}


