package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.dao.AguacateDatosGeneralesDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.AguacateDatosGeneralesDAONIO;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;
import javafx.scene.control.Alert;

import java.util.List;

public class AguacateDatosGeneralesBsn {

    private AguacateDatosGeneralesDAO aguacateDatosGeneralesDAO;

    public AguacateDatosGeneralesBsn() {
        this.aguacateDatosGeneralesDAO = new AguacateDatosGeneralesDAONIO();
    }

    public void registrarDatosGeneralesAguacate(String precioKgIngresadoAgt, String precioSemillaIngresadoAgt, String ubicacionIngresadoAgt, String codigoGeneralIngresadoAgt) throws DatosIncorrectosException {
        try {
            Float.parseFloat(precioKgIngresadoAgt);
        } catch (NumberFormatException exception) {
            throw new DatosIncorrectosException();
        }

        try {
            Float.parseFloat(precioSemillaIngresadoAgt);
        } catch (NumberFormatException exception) {
            throw new DatosIncorrectosException();
        }

        if (codigoGeneralIngresadoAgt.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (ubicacionIngresadoAgt.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        DatosGeneralesCultivos datosGeneralesAguacateIngresados = new DatosGeneralesCultivos(Float.parseFloat(precioKgIngresadoAgt), Float.parseFloat(precioSemillaIngresadoAgt), ubicacionIngresadoAgt, codigoGeneralIngresadoAgt);
        this.aguacateDatosGeneralesDAO.registrarDatosGeneralesAguacate(datosGeneralesAguacateIngresados);
    }

    public List<DatosGeneralesCultivos> consultarDatosGeneralesAguacates() throws DatosGeneralesRequeridosException {
        List<DatosGeneralesCultivos> datosGeneralesAguacate = aguacateDatosGeneralesDAO.consultarDatosGeneralesAguacates();
        if (datosGeneralesAguacate.isEmpty()){
            throw new DatosGeneralesRequeridosException();
        }
        return datosGeneralesAguacate;
    }

    public float consultarPrecioKg() throws DatosGeneralesRequeridosException{
        float precioKg = aguacateDatosGeneralesDAO.consultarPrecioKg();
        if (precioKg == 0){
            throw new DatosGeneralesRequeridosException();
        }
        return precioKg;
    }
}
