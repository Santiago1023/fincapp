package co.edu.udea.tecnicas.fincapp.bsn.exception;

public class DatosIncorrectosException extends Exception{
    public DatosIncorrectosException(){
        super ("Los datos estan incorrectos");
    }
}
