package co.edu.udea.tecnicas.fincapp.bsn.exception;

public class RegistroGeneralYaExisteException extends Exception {

    public RegistroGeneralYaExisteException(){
        super("Ya existe un registro general con este codigo");
    }
}
