package co.edu.udea.tecnicas.fincapp.bsn.exception;

public class FaltaRegistrarMarranosException extends  Exception{
    public FaltaRegistrarMarranosException(){
        super("Falta registrar marranos del grupo que contienen este codigo general");
    }
}
