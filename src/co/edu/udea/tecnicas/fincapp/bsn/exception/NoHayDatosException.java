package co.edu.udea.tecnicas.fincapp.bsn.exception;

public class NoHayDatosException extends Exception{
    public NoHayDatosException(){
        super("No hay existencia de estos datos");
    }
}
