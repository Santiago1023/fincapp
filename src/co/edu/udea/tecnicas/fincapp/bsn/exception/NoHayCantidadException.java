package co.edu.udea.tecnicas.fincapp.bsn.exception;

public class NoHayCantidadException extends Exception{
    public NoHayCantidadException(){
        super("La cantidad que se quiere vender exede la disponibilidad del producto");
    }
}
