package co.edu.udea.tecnicas.fincapp.bsn.exception;

import co.edu.udea.tecnicas.fincapp.controller.DatosGeneralesTomatesController;

public class DatosGeneralesRequeridosException extends Exception {
    public DatosGeneralesRequeridosException(){
        super("Los datos generales son requeridos antes de registrar un grupo de cultivo");
    }
}
