package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.dao.VentaDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.VentaDAONIO;
import co.edu.udea.tecnicas.fincapp.model.Venta;

import java.time.LocalDate;
import java.util.List;

public class VentaBsn {

    private VentaDAO ventaDAO;

    public VentaBsn(){this.ventaDAO = new VentaDAONIO();
    }

    public void registrarVenta(String codigoFactura, String producto, int cantidad, float precioVenta, LocalDate fecha){
        Venta ventaIngresada = new Venta(codigoFactura,producto,cantidad,precioVenta,fecha);
        this.ventaDAO.registrarVenta(ventaIngresada);
    }

    public List<Venta> consultarVentas() throws NoHayDatosException{
        List<Venta> ventas = ventaDAO.consultarVentas();
        if (ventas.isEmpty()){
            throw new NoHayDatosException();
        }
        return ventas;
    }

    public List<Venta> consultarVentasEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal) throws NoHayDatosException{
        List<Venta> ventas = ventaDAO.consultatVentasEntreFechas(fechaInicio,fechaFinal);
        if (ventas.isEmpty()){
            throw new NoHayDatosException();
        }
        return ventas;
    }

    public List<Venta> consultarVentasDias(int numeroDias) throws NoHayDatosException{
        List<Venta> ventas = ventaDAO.consultatVentasDias(numeroDias);
        if (ventas.isEmpty()){
            throw new NoHayDatosException();
        }
        return ventas;
    }

    public List<Venta> consultarVentasMes(int mes) throws NoHayDatosException{
        List<Venta> ventas = ventaDAO.consultarVentasMes(mes);
        if (ventas.isEmpty()){
            throw new NoHayDatosException();
        }
        return ventas;
    }

    public float valorVentasTotales(){
        return ventaDAO.valorVentasTotales();
    }

    public float valorVentasTotalesEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal){
        return ventaDAO.valorVentasEntreFechas(fechaInicio,fechaFinal);
    }

    public float valorVentasDias(int numeroDias){
        return ventaDAO.valorVentasDias(numeroDias);
    }

    public float valorVentasMes(int mes){
        return ventaDAO.valorVentasMes(mes);
    }

    public int codigoUltimaVenta(){
        return ventaDAO.codigoUltimaFactura();
    }
}
