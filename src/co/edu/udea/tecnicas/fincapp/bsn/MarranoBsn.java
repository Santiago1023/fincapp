package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.FaltaRegistrarMarranosException;
import co.edu.udea.tecnicas.fincapp.dao.GrupoAnimalDAO;
import co.edu.udea.tecnicas.fincapp.dao.MarranoDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.GrupoAnimalDAONIO;
import co.edu.udea.tecnicas.fincapp.dao.impl.MarranoDAONIO;
import co.edu.udea.tecnicas.fincapp.model.Marrano;

import java.util.List;

public class MarranoBsn {


    // atributo de tipo DAO----> se crea de tipo interface para hacer que la referencia sea DAONIO, DAONIOV2, DAOLIST, La ligadura de markov que decía el profe
    private MarranoDAO marranoDAO;

    private GrupoAnimalDAO marranoGrupoAnimalDao;

    //creamos la instancia para la conexion de bsn con DAO
    public MarranoBsn(){
        this.marranoDAO = new MarranoDAONIO();
        this.marranoGrupoAnimalDao = new GrupoAnimalDAONIO();
    }

    // tireselo al DAO
    public void registrarMarrano(Marrano marrano) {
        this.marranoDAO.registrarMarrano(marrano);
    }

    public List<Marrano> consultarMarranos(){
        return marranoDAO.consultarMarranos();
    }

    public void verificarCantidad() throws FaltaRegistrarMarranosException {
       int cantidad = this.marranoDAO.verificarCantidad(this.marranoGrupoAnimalDao.codigoGeneral());
       if(cantidad != this.marranoGrupoAnimalDao.cantidadGeneral()){
           throw  new FaltaRegistrarMarranosException();

       }
    }
}
