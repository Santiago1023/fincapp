package co.edu.udea.tecnicas.fincapp.bsn;
import co.edu.udea.tecnicas.fincapp.dao.impl.GallinaDAONIO;
import co.edu.udea.tecnicas.fincapp.dao.GallinaDAO;
import co.edu.udea.tecnicas.fincapp.model.Gallina;

import java.util.List;

public class GallinaBsn {



    // atributo de tipo DAO----> se crea de tipo interface para hacer que la referencia sea DAONIO, DAONIOV2, DAOLIST, La ligadura de markov que decía el profe
    private GallinaDAO gallinaDAO;


    //creamos la instancia para la conexion de bsn con DAO
    public GallinaBsn(){
        this.gallinaDAO = new GallinaDAONIO();
    }

    // tireselo al DAO
    public void registrarGallina(Gallina gallina) {
        this.gallinaDAO.registrarGallina(gallina);
    }

    public List<Gallina> consultarGallinas(){
        return gallinaDAO.consultarGallinas();
    }
}
