package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.dao.GrupoAnimalDAO;
import co.edu.udea.tecnicas.fincapp.dao.MarranoDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.GrupoAnimalDAONIO;
import co.edu.udea.tecnicas.fincapp.dao.impl.MarranoDAONIO;

// todo hacer las conexiones con los dos negocios marrano y registrar grupo

public class MarranoCompletoBsn {

    private MarranoDAO marranoDAO;
    private GrupoAnimalDAO grupoAnimalDAO;

    public MarranoCompletoBsn(){
        this.grupoAnimalDAO = new GrupoAnimalDAONIO();
        this.marranoDAO = new MarranoDAONIO();
    }

}
