package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.dao.TomateDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.TomateDAONIO;
import co.edu.udea.tecnicas.fincapp.model.Tomate;
import co.edu.udea.tecnicas.fincapp.model.Tomate;

import java.time.LocalDate;
import java.util.List;

public class TomatesBsn {

    private TomateDAO tomateDAO;

    public TomatesBsn() {
        this.tomateDAO = new TomateDAONIO();
    }

    public void registrarTomate(String codigoLote, String fertilizante, String abono, String insecticida, LocalDate fecha, String estado) {
        //Validar otra vez
        Tomate tomateIngresado = new Tomate(codigoLote, fertilizante, abono, insecticida,fecha,estado);
        this.tomateDAO.registrarTomate(tomateIngresado);
    }

    public List<Tomate> consultarTomates() throws NoHayDatosException {
        List<Tomate> tomates = tomateDAO.consultarTomates();
        if (tomates.isEmpty()){
            throw new NoHayDatosException();
        }
        return tomates;
    }

    public List<Tomate> consultarTomatesIngresados() throws NoHayDatosException{
        List<Tomate> tomates = tomateDAO.consultarTomatesDisponibles();
        if (tomates.isEmpty()){
            throw new NoHayDatosException();
        }
        return  tomates;
    }

    public List<Tomate> consultarTomatesVendidos() throws NoHayDatosException{
        List<Tomate> tomates = tomateDAO.consultarTomatesVendidos();
        if (tomates.isEmpty()){
            throw new NoHayDatosException();
        }
        return  tomates;
    }

    public void venderTomates(int cantidadAVender) throws NoHayCantidadException {
        tomateDAO.venderTomate(cantidadAVender);
    }

    public int codigoUltimoTomateRegistrado() {
        return tomateDAO.codigoUltimoTomateRegistrado();
    }

    public List<String> tiposFertilizantesRegistradosTmt() throws NoHayDatosException {
        List<String> tiposFertilizantesRegistradosTmt = tomateDAO.tiposFertilizantesRegistradosTmt();
        if (tiposFertilizantesRegistradosTmt.isEmpty()) {
            throw new NoHayDatosException();
        }
        return tiposFertilizantesRegistradosTmt;
    }

    public List<String> tiposAbonosRegistradosTmt() throws NoHayDatosException {
        List<String> tiposAbonos = tomateDAO.tiposAbonosRegistradosTmt();
        if (tiposAbonos.isEmpty()) {
            throw new NoHayDatosException();
        }
        return tiposAbonos;
    }

    public List<String> tiposInsecticidasRegistradosTmt() throws NoHayDatosException {
        List<String> tiposInsecticidas = tomateDAO.tiposInsecticidasRegistradosTmt();
        if (tiposInsecticidas.isEmpty()) {
            throw new NoHayDatosException();
        }
        return tiposInsecticidas;
    }

}
