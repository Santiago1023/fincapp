package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.dao.AguacateDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.AguacateDAONIO;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;
import javafx.scene.control.Alert;

import java.time.LocalDate;
import java.util.List;

public class AguacatesBsn {

    private AguacateDAO aguacateDAO;

    public AguacatesBsn(){
        this.aguacateDAO = new AguacateDAONIO();
    }

    public void registrarAguacate(String codigoLote, String fertilizante, String abono, String insecticida, LocalDate fecha, String estado) throws DatosIncorrectosException {
        //Validacion de que los datos tengan un valor o sean del tipo requerido
        if (codigoLote.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (fertilizante.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (abono.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (insecticida.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (estado.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        Aguacate aguacateIngresado = new Aguacate(codigoLote,fertilizante,abono,insecticida,fecha,estado);
        this.aguacateDAO.registrarAguacate(aguacateIngresado);
    }

    public List<Aguacate> consultarAguacates() throws NoHayDatosException{
        List<Aguacate> aguacates = aguacateDAO.consultarAguacates();
        if (aguacates.isEmpty()){
            throw new NoHayDatosException();
        }
        return  aguacates;
    }

    public List<Aguacate> consultarAguacatesIngresados() throws NoHayDatosException{
        List<Aguacate> aguacates = aguacateDAO.consultarAguacatesDisponibles();
        if (aguacates.isEmpty()){
            throw new NoHayDatosException();
        }
        return  aguacates;
    }

    public List<Aguacate> consultarAguacatesVendidos() throws NoHayDatosException{
        List<Aguacate> aguacates = aguacateDAO.consultarAguacatesVendidos();
        if (aguacates.isEmpty()){
            throw new NoHayDatosException();
        }
        return  aguacates;
    }

    public void venderAguacates(int cantidadAVender) throws NoHayCantidadException {
        aguacateDAO.venderAguacates(cantidadAVender);
    }

    //No deberia tirar exception porque devuelve un valor siempre
    public int codigoUltimoAguacateRegistrado() {
        return aguacateDAO.codigoUltimoAguacateRegistrado();
    }

    public List<String> tiposFertilizantesRegistradosAgt() throws NoHayDatosException {
        List<String> tiposFertilizantesRegistradosAgt = aguacateDAO.tiposFertilizantesRegistradosAgt();
        if (tiposFertilizantesRegistradosAgt.isEmpty()){
            throw new NoHayDatosException();
        }
        return tiposFertilizantesRegistradosAgt;
    }

    public List<String> tiposAbonosRegistradosAgt() throws NoHayDatosException {
        List<String> tiposAbonosRegistradosAgt = aguacateDAO.tiposAbonosRegistradosAgt();
        if (tiposAbonosRegistradosAgt.isEmpty()){
            throw new NoHayDatosException();
        }
        return tiposAbonosRegistradosAgt;
    }

    public List<String> tiposInsecticidasRegistradosAgt() throws NoHayDatosException {
        List<String> tiposInsecticidasRegistradosAgt = aguacateDAO.tiposInsecticidasRegistradosAgt();
        if (tiposInsecticidasRegistradosAgt.isEmpty()){
            throw new NoHayDatosException();
        }
        return tiposInsecticidasRegistradosAgt;
    }


}
