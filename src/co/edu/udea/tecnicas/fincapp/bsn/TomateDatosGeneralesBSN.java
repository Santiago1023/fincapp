package co.edu.udea.tecnicas.fincapp.bsn;

import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.dao.TomateDatosGeneralesDAO;
import co.edu.udea.tecnicas.fincapp.dao.impl.TomateDatosGeneralesDAONIO;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;

import java.util.List;

public class TomateDatosGeneralesBSN {

    private TomateDatosGeneralesDAO tomateDatosGeneralesDAO;

    public TomateDatosGeneralesBSN(){
        this.tomateDatosGeneralesDAO = new TomateDatosGeneralesDAONIO();
    }

    public void registrarDatosGeneralesTomate(String precioKgIngresadoTmt, String precioSemillaIngresadoTmt, String ubicacionIngresadoTmt, String codigoGeneralIngresadoTmt) throws DatosIncorrectosException {

        try {
            Float.parseFloat(precioKgIngresadoTmt);
        } catch (NumberFormatException exception) {
            throw new DatosIncorrectosException();
        }

        try {
            Float.parseFloat(precioSemillaIngresadoTmt);
        } catch (NumberFormatException exception) {
            throw new DatosIncorrectosException();
        }

        if (codigoGeneralIngresadoTmt.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        if (ubicacionIngresadoTmt.isEmpty()) {
            throw new DatosIncorrectosException();
        }

        DatosGeneralesCultivos datosGeneralesTomateIngresados = new DatosGeneralesCultivos(Float.parseFloat(precioKgIngresadoTmt), Float.parseFloat(precioSemillaIngresadoTmt), ubicacionIngresadoTmt, codigoGeneralIngresadoTmt);

        this.tomateDatosGeneralesDAO.registrarDatosGeneralesTomates(datosGeneralesTomateIngresados);
    }

    public List<DatosGeneralesCultivos> consultarDatosGeneralesTomates()  throws DatosGeneralesRequeridosException {
        List<DatosGeneralesCultivos> datosGeneralesTomate = tomateDatosGeneralesDAO.consultarDatosGeneralesTomates();
        if (datosGeneralesTomate.isEmpty()){
            throw new DatosGeneralesRequeridosException();
        }
        return datosGeneralesTomate;
    }

    public float consultarPrecioKg() throws DatosGeneralesRequeridosException{
        float precioKg = tomateDatosGeneralesDAO.consultarPrecioKg();
        if (precioKg == 0){
            throw new DatosGeneralesRequeridosException();
        }
        return precioKg;
    }
}
