package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.Gallina;

import java.util.List;

public interface GallinaDAO {

    void registrarGallina(Gallina gallina);
    List<Gallina> consultarGallinas();
}
