package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.Venta;

import java.time.LocalDate;
import java.util.List;

public interface VentaDAO {
    void registrarVenta(Venta venta);
    List<Venta> consultarVentas();
    List<Venta> consultatVentasEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal); //Formato yyyy-mm-dd
    List<Venta> consultatVentasDias(int numeroDias);
    List<Venta> consultarVentasMes(int mes);
    int codigoUltimaFactura();
    float valorVentasTotales();
    float valorVentasEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal);
    float valorVentasDias(int numeroDias);
    float valorVentasMes(int mes);

}
