package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.dao.TomateDAO;
import co.edu.udea.tecnicas.fincapp.model.Tomate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class TomateDAONIO implements TomateDAO {

    private final static String NOMBRE_ARCHIVO = "tomates";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static String VENDIDO = "VENDIDO";
    private final static String INGRESADO = "INGRESADO";

    private final static Integer LONGITUD_REGISTRO = 192;
    private final static Integer LONGITUD_CODIGO = 20;
    private final static Integer LONGITUD_FERTILIZANTE = 50;
    private final static Integer LONGITUD_ABONO = 50;
    private final static Integer LONGITUD_INSECTICIDA = 50;
    private final static Integer LONGITUD_FECHA = 10;
    private final static Integer LONGITUD_ESTADO = 12;  //vendido ingresado descompuesto

    public TomateDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarTomate(Tomate tomate) {
        //Se obtiene la representacion en string del estudiante
        String registro = parseTomateString(tomate);
        //Se extraen los bytes del registro
        byte[] datoRegistro = registro.getBytes();
        //Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        //Se abre un canal hacia el archivo
        //WRITE para cambiar el archivo, APPEND para agregarle
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            //Se escriben los datos en el buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<Tomate> consultarTomates() {
        List<Tomate> tomates = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Tomate tomate = parseBufferTomate(registro);
                tomates.add(tomate);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tomates;
    }

    @Override
    public List<Tomate> consultarTomatesDisponibles() {
        List<Tomate> tomatesDisponibles = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Tomate tomate = parseBufferTomate(registro);
                if (tomate.getEstado().equals(INGRESADO)){
                    tomatesDisponibles.add(tomate);
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tomatesDisponibles;
    }

    @Override
    public List<Tomate> consultarTomatesVendidos() {
        List<Tomate> tomatesDisponibles = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Tomate tomate = parseBufferTomate(registro);
                if (tomate.getEstado().equals(VENDIDO)){
                    tomatesDisponibles.add(tomate);
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tomatesDisponibles;
    }

    @Override
    public void venderTomate(int cantidadVentas) throws NoHayCantidadException {
        List<Tomate> tomatesDisponibles = consultarTomatesDisponibles(); //Siempre estarian en orden
        if (tomatesDisponibles.size()<cantidadVentas){
            throw new NoHayCantidadException();
        }
        int cambiados=0;
        for (Tomate tmt:tomatesDisponibles) {
            if (cambiados>=cantidadVentas){
                break;
            }
            int posicion = 0;
            try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
                ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
                while (sbc.read(buffer) > 0) {
                    buffer.rewind();
                    CharBuffer registro = Charset.defaultCharset().decode(buffer);
                    Tomate tomate = parseBufferTomate(registro);
                    if (tomate.getCodigoLote().equals(tmt.getCodigoLote())){
                        int posicionEncontro = posicion*LONGITUD_REGISTRO;
                        tomate.setEstado(VENDIDO);
                        cambiarEstado(tomate,posicionEncontro);
                        cambiados++;
                        break;
                    }
                    buffer.flip();
                    posicion++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<String> tiposFertilizantesRegistradosTmt() {
        List<String> tiposFertilizantesRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, LONGITUD_CODIGO, LONGITUD_FERTILIZANTE);
                if (tiposFertilizantesRegistrados.isEmpty()) {
                    tiposFertilizantesRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposFertilizantesRegistrados) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo) {
                        tiposFertilizantesRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposFertilizantesRegistrados;
    }

    @Override
    public List<String> tiposAbonosRegistradosTmt() {
        List<String> tiposAbonosRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, (LONGITUD_CODIGO + LONGITUD_FERTILIZANTE), LONGITUD_ABONO);
                if (tiposAbonosRegistrados.isEmpty()) {
                    tiposAbonosRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposAbonosRegistrados) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo) {
                        tiposAbonosRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposAbonosRegistrados;
    }

    @Override
    public List<String> tiposInsecticidasRegistradosTmt() {
        List<String> tiposInsecticidasRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, (LONGITUD_CODIGO + LONGITUD_FERTILIZANTE + LONGITUD_ABONO), LONGITUD_INSECTICIDA);
                if (tiposInsecticidasRegistrados.isEmpty()) {
                    tiposInsecticidasRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposInsecticidasRegistrados) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo) {
                        tiposInsecticidasRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposInsecticidasRegistrados;
    }

    @Override
    public int codigoUltimoTomateRegistrado() {
        int ultimoCodigo = 0;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimoCodigo = ultimoValorCodigo(registro);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return ultimoCodigo;
    }

    private String tiposRegistrados(CharBuffer registro, Integer LONGITUD_INICIAL, Integer LONGITUD_FINAL) {
        registro.position(LONGITUD_INICIAL);
        registro = registro.slice();
        String dato = registro.subSequence(0, LONGITUD_FINAL).toString().trim();
        return dato;
    }

    private int ultimoValorCodigo(CharBuffer registro) {
        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        StringBuilder ultimoCodigo = new StringBuilder();
        for (int i = 0; i < codigo.length(); i++) {
            if (Character.isDigit(codigo.charAt(i))) {
                ultimoCodigo.append(codigo.charAt(i));
            }
        }
        return Integer.parseInt(ultimoCodigo.toString());
    }

    private Tomate parseBufferTomate(CharBuffer registro) {

        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        registro.position(LONGITUD_CODIGO);
        registro = registro.slice();

        String fertilizante = registro.subSequence(0, LONGITUD_FERTILIZANTE).toString().trim();
        registro.position(LONGITUD_FERTILIZANTE);
        registro = registro.slice();

        String abono = registro.subSequence(0, LONGITUD_ABONO).toString().trim();
        registro.position(LONGITUD_ABONO);
        registro = registro.slice();

        String insecticida = registro.subSequence(0, LONGITUD_INSECTICIDA).toString().trim();
        registro.position(LONGITUD_INSECTICIDA);
        registro = registro.slice();

        LocalDate fecha = LocalDate.parse(registro.subSequence(0, LONGITUD_FECHA).toString().trim());
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();

        String estado = registro.subSequence(0, LONGITUD_ESTADO).toString().trim();
        registro.position(LONGITUD_ESTADO);
        registro = registro.slice();

        Tomate tomate = new Tomate(codigo, fertilizante, abono, insecticida, fecha, estado);

        return tomate;
    }

    /**
     * Recibe un objeto tipo cultivo y con base en los tamaños de los campos retorna un string acompañado de las variables estaticas de la clase tomate
     *
     * @param tomate
     * @return
     */
    private String parseTomateString(Tomate tomate) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampos(tomate.getCodigoLote(), LONGITUD_CODIGO))
                .append(completarCampos(tomate.getFertilizante(), LONGITUD_FERTILIZANTE))
                .append(completarCampos(tomate.getAbono(), LONGITUD_ABONO))
                .append(completarCampos(tomate.getInsecticida(), LONGITUD_INSECTICIDA))
                .append(completarCampos(tomate.getFecha().toString(),LONGITUD_FECHA))
                .append(completarCampos(tomate.getEstado(), LONGITUD_ESTADO));
        return sb.toString();
    }

    private String completarCampos(String dato, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", dato);
    }

    private void cambiarEstado(Tomate tomate, int posicionACambiar){
        String tomateString = parseTomateString(tomate);
        byte[] datoRegistro = tomateString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer,posicionACambiar);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
