package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.dao.AguacateDAO;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class AguacateDAONIO implements AguacateDAO {

    private final static String NOMBRE_ARCHIVO = "aguacates";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static String VENDIDO = "VENDIDO";
    private final static String INGRESADO = "INGRESADO";

    private final static Integer LONGITUD_REGISTRO = 192;
    private final static Integer LONGITUD_CODIGO = 20;
    private final static Integer LONGITUD_FERTILIZANTE = 50;
    private final static Integer LONGITUD_ABONO = 50;
    private final static Integer LONGITUD_INSECTICIDA = 50;
    private final static Integer LONGITUD_FECHA = 10;
    private final static Integer LONGITUD_ESTADO = 12;  //vendido ingresado descompuesto

    public AguacateDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarAguacate(Aguacate aguacate) {
        //Se obtiene la representacion en string del aguacate
        String registro = parseAguacateString(aguacate);
        //Se extraen los bytes del registro
        byte[] datoRegistro = registro.getBytes();
        //Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        //Se abre un canal hacia el archivo
        //WRITE para cambiar el archivo, APPEND para agregarle
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            //Se escriben los datos en el buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<Aguacate> consultarAguacates() {
        List<Aguacate> aguacates = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Aguacate aguacate = parseBufferAguacate(registro);
                aguacates.add(aguacate);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return aguacates;
    }

    @Override
    public List<Aguacate> consultarAguacatesDisponibles() {
        List<Aguacate> aguacatesDisponibles = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Aguacate aguacate = parseBufferAguacate(registro);
                if (aguacate.getEstado().equals(INGRESADO)){
                    aguacatesDisponibles.add(aguacate);
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return aguacatesDisponibles;
    }

    @Override
    public List<Aguacate> consultarAguacatesVendidos() {
        List<Aguacate> aguacatesDisponibles = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Aguacate aguacate = parseBufferAguacate(registro);
                if (aguacate.getEstado().equals(VENDIDO)){
                    aguacatesDisponibles.add(aguacate);
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return aguacatesDisponibles;
    }

    @Override
    public void venderAguacates(int cantidadVentas) throws NoHayCantidadException {
        List<Aguacate> aguacatesDisponibles = consultarAguacatesDisponibles(); //Siempre estarian en orden
        if (aguacatesDisponibles.size()<cantidadVentas){
            throw new NoHayCantidadException();
        }
        int cambiados=0;
        for (Aguacate agt:aguacatesDisponibles) {
            if (cambiados>=cantidadVentas){
                break;
            }
            int posicion = 0;
            try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
                ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
                while (sbc.read(buffer) > 0) {
                    buffer.rewind();
                    CharBuffer registro = Charset.defaultCharset().decode(buffer);
                    Aguacate aguacate = parseBufferAguacate(registro);
                    if (aguacate.getCodigoLote().equals(agt.getCodigoLote())){
                        int posicionEncontro = posicion*LONGITUD_REGISTRO;
                        aguacate.setEstado(VENDIDO);
                        cambiarEstado(aguacate,posicionEncontro);
                        cambiados++;
                        break;
                    }
                    buffer.flip();
                    posicion++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<String> tiposFertilizantesRegistradosAgt() {
        List<String> tiposFertilizantesRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, LONGITUD_CODIGO, LONGITUD_FERTILIZANTE);
                if (tiposFertilizantesRegistrados.isEmpty()) {
                    tiposFertilizantesRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposFertilizantesRegistrados ) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo){
                        tiposFertilizantesRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposFertilizantesRegistrados;
    }

    @Override
    public List<String> tiposAbonosRegistradosAgt() {
        List<String> tiposAbonosRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, (LONGITUD_CODIGO+LONGITUD_FERTILIZANTE), LONGITUD_ABONO);
                if (tiposAbonosRegistrados.isEmpty()) {
                    tiposAbonosRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposAbonosRegistrados ) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo){
                        tiposAbonosRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposAbonosRegistrados;
    }

    @Override
    public List<String> tiposInsecticidasRegistradosAgt() {
        List<String> tiposInsecticidasRegistrados = new ArrayList();
        Boolean nuevo = false;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String tipoRegistrado = tiposRegistrados(registro, (LONGITUD_CODIGO+LONGITUD_FERTILIZANTE+LONGITUD_ABONO), LONGITUD_INSECTICIDA);
                if (tiposInsecticidasRegistrados.isEmpty()) {
                    tiposInsecticidasRegistrados.add(tipoRegistrado);
                } else {
                    for (String tipo : tiposInsecticidasRegistrados ) {
                        if (tipo.equals(tipoRegistrado)) {
                            nuevo = false;
                            break;
                        } else {
                            nuevo = true;
                        }
                    }
                    if (nuevo){
                        tiposInsecticidasRegistrados.add(tipoRegistrado);
                    }
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return tiposInsecticidasRegistrados;
    }

    @Override
    public int codigoUltimoAguacateRegistrado() {
        int ultimoCodigo = 0;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimoCodigo = ultimoValorCodigo(registro);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return ultimoCodigo;
    }

    private String tiposRegistrados(CharBuffer registro, Integer LONGITUD_INICIAL, Integer LONGITUD_FINAL) {
        registro.position(LONGITUD_INICIAL);
        registro = registro.slice();
        String dato = registro.subSequence(0, LONGITUD_FINAL).toString().trim();
        return dato;
    }

    private int ultimoValorCodigo(CharBuffer registro) {
        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        StringBuilder ultimoCodigo = new StringBuilder();
        for (int i = 0; i < codigo.length(); i++) {
            if (Character.isDigit(codigo.charAt(i))) {
                ultimoCodigo.append(codigo.charAt(i));
            }
        }
        return Integer.parseInt(ultimoCodigo.toString());
    }

    private Aguacate parseBufferAguacate(CharBuffer registro) {

        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        registro.position(LONGITUD_CODIGO);
        registro = registro.slice();

        String fertilizante = registro.subSequence(0, LONGITUD_FERTILIZANTE).toString().trim();
        registro.position(LONGITUD_FERTILIZANTE);
        registro = registro.slice();

        String abono = registro.subSequence(0, LONGITUD_ABONO).toString().trim();
        registro.position(LONGITUD_ABONO);
        registro = registro.slice();

        String insecticida = registro.subSequence(0, LONGITUD_INSECTICIDA).toString().trim();
        registro.position(LONGITUD_INSECTICIDA);
        registro = registro.slice();

        LocalDate fecha = LocalDate.parse(registro.subSequence(0,LONGITUD_FECHA).toString().trim());
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();

        String estado = registro.subSequence(0,LONGITUD_ESTADO).toString().trim();
        registro.position(LONGITUD_ESTADO);
        registro = registro.slice();


        Aguacate aguacate = new Aguacate(codigo, fertilizante, abono, insecticida,fecha,estado);

        return aguacate;
    }

    /**
     * Recibe un objeto tipo cultivo y con base en los tamaños de los campos retorna un string acompañado de las variables estaticas de la clase aguacate
     *
     * @param aguacate
     * @return
     */
    private String parseAguacateString(Aguacate aguacate) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampos(aguacate.getCodigoLote(), LONGITUD_CODIGO))
                .append(completarCampos(aguacate.getFertilizante(), LONGITUD_FERTILIZANTE))
                .append(completarCampos(aguacate.getAbono(), LONGITUD_ABONO))
                .append(completarCampos(aguacate.getInsecticida(), LONGITUD_INSECTICIDA))
                .append(completarCampos(aguacate.getFecha().toString(),LONGITUD_FECHA))
                .append(completarCampos(aguacate.getEstado(), LONGITUD_ESTADO));
        return sb.toString();
    }

    private String completarCampos(String dato, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", dato);
    }

    private void cambiarEstado(Aguacate aguacate, int posicionACambiar){
        String aguacateString = parseAguacateString(aguacate);
        byte[] datoRegistro = aguacateString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            fc.write(byteBuffer,posicionACambiar);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
