package co.edu.udea.tecnicas.fincapp.dao.impl;
import co.edu.udea.tecnicas.fincapp.dao.GallinaDAO;
import co.edu.udea.tecnicas.fincapp.model.Gallina;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import static java.nio.file.StandardOpenOption.APPEND;

public class GallinaDAONIO implements GallinaDAO {



    private final static String NOMBRE_ARCHIVO = "gallinas";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static Integer LONGITUD_REGISTRO = 80;
    private final static Integer LONGITUD_PESO = 4;
    private final static Integer LONGITUD_ALIMENTO = 15;
    private final static Integer LONGITUD_VACUNA = 20;
    private final static Integer LONGITUD_PRECIOKG = 5;
    private final static Integer LONGITUD_ESTADOSALUD = 15;
    private final static Integer LONGITUD_EDAD = 4;
    private final static Integer LONGITUD_LUGAR = 17;


    public GallinaDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }


    @Override
    public void registrarGallina(Gallina gallina) {
        // Se obtiene la representación en String del marrano
        String registro = parseGallinaString(gallina);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try(FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * Recibe un objeto de tipo marrano y, con base en los tamaños de los campos, retorna un string
     *
     * @param gallina
     * @return
     */
    private String parseGallinaString(Gallina gallina) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampo(gallina.getPeso(), LONGITUD_PESO))
                .append(completarCampo(gallina.getAlimento(), LONGITUD_ALIMENTO))
                .append(completarCampo(gallina.getVacuna(), LONGITUD_VACUNA))
                .append(completarCampo(gallina.getPrecioKg(), LONGITUD_PRECIOKG))
                .append(completarCampo(gallina.getEstadoSalud(), LONGITUD_ESTADOSALUD))
                .append(completarCampo(gallina.getEdad(), LONGITUD_EDAD))
                .append(completarCampo(gallina.getLugar(), LONGITUD_LUGAR));
        return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudCampo) {
        // si el valor del campo supera su longitud, se recorta hasta la longitud máxima permitida
        if (valor.length() > longitudCampo) {
            return valor.substring(0, longitudCampo);
        }
        // regex: Regular expresion
        return String.format("%1$" + longitudCampo + "s", valor);
    }




    @Override
    public List<Gallina> consultarGallinas() {
        List<Gallina> gallinas = new ArrayList<>();
        // se abre un canal hacia el archivo para leer bytes
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // se encarga de capturar 70 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // lee paquetes de bytes hasta que llegue al final del archivo
            while (sbc.read(buffer) > 0) {
                // ubica el apuntador del buffer en la posición inicial
                buffer.rewind();
                // decodifica los bytes usando el juego de caracteres por defecto del sistema operativo
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Gallina gallina = parseBufferGallina(registro);
                gallinas.add(gallina);
                //prepara al buffer para leer bytes del disco de nuevo
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return gallinas;
    }

    private Gallina parseBufferGallina(CharBuffer registro) {
        Gallina gallina = new Gallina();

        String peso = registro.subSequence(0, LONGITUD_PESO).toString().trim();
        gallina.setPeso(peso);
        registro.position(LONGITUD_PESO);
        registro = registro.slice();

        String alimento = registro.subSequence(0, LONGITUD_ALIMENTO).toString().trim();
        gallina.setAlimento(alimento);
        registro.position(LONGITUD_ALIMENTO);
        registro = registro.slice();

        String vacuna = registro.subSequence(0, LONGITUD_VACUNA).toString().trim();
        gallina.setVacuna(vacuna);
        registro.position(LONGITUD_VACUNA);
        registro = registro.slice();

        String precioKg = registro.subSequence(0, LONGITUD_PRECIOKG).toString().trim();
        gallina.setPrecioKg(precioKg);
        registro.position(LONGITUD_PRECIOKG);
        registro = registro.slice();

        String estadoSalud = registro.subSequence(0, LONGITUD_ESTADOSALUD).toString().trim();
        gallina.setEstadoSalud(estadoSalud);
        registro.position(LONGITUD_ESTADOSALUD);
        registro = registro.slice();

        String edad = registro.subSequence(0, LONGITUD_EDAD).toString().trim();
        gallina.setEdad(edad);
        registro.position(LONGITUD_EDAD);
        registro = registro.slice();

        String lugar = registro.toString().trim();
        gallina.setLugar(lugar);
        return gallina;
    }


}
