package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.dao.GrupoAnimalDAO;
import co.edu.udea.tecnicas.fincapp.model.GrupoAnimal;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static java.nio.file.StandardOpenOption.APPEND;

public class GrupoAnimalDAONIO implements GrupoAnimalDAO {

    private final static String NOMBRE_ARCHIVO = "grupoAnimal";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static Integer LONGITUD_REGISTRO = 115;
    private final static Integer LONGITUD_FECHA = 15;
    private final static Integer LONGITUD_CANTIDAD = 5;
    private final static Integer LONGITUD_TIPO = 10;
    private final static Integer LONGITUD_DIETA = 25;
    private final static Integer LONGITUD_PRECIOPORKG = 5;
    private final static Integer LONGITUD_UBICACION = 30;
    private final static Integer LONGITUD_CODIGO = 25;

    public GrupoAnimalDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarGrupoAnimal(GrupoAnimal grupoAnimal) {

        // Se obtiene la representación en String del grupo
        String registro = parseGrupoAnimalString(grupoAnimal);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }


    }

    /**
     * Recibe un objeto de tipo grupoAnimal y, con base en los tamaños de los campos, retorna un string
     *
     * @param grupoAnimal
     * @return
     */
    private String parseGrupoAnimalString(GrupoAnimal grupoAnimal) {
        StringBuilder sb = new StringBuilder();
        String fechaString =  grupoAnimal.getFechaRegistro().format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
        String cantidadString = Integer.toString(grupoAnimal.getCantidad());
        String precioString = Double.toString(grupoAnimal.getPrecioKg());
        sb.append(completarCampo(fechaString, LONGITUD_FECHA))
                .append(completarCampo(cantidadString, LONGITUD_CANTIDAD))
                .append(completarCampo(grupoAnimal.getTipo(), LONGITUD_TIPO))
                .append(completarCampo(grupoAnimal.getDieta(), LONGITUD_DIETA))
                .append(completarCampo(precioString, LONGITUD_PRECIOPORKG))
                .append(completarCampo(grupoAnimal.getUbicacion(), LONGITUD_UBICACION))
                .append(completarCampo(grupoAnimal.getCodigo(), LONGITUD_CODIGO));
        return sb.toString();

    }

    private String completarCampo(String valor, Integer longitudCampo) {
        // si el valor del campo supera su longitud, se recorta hasta la longitud máxima permitida
        if (valor.length() > longitudCampo) {
            return valor.substring(0, longitudCampo);
        }
        // regex: Regular expresion
        return String.format("%1$" + longitudCampo + "s", valor);
    }

    @Override
    public List<GrupoAnimal> mostrarGrupoAnimal() {
        List<GrupoAnimal> grupoAnimales = new ArrayList<>();
        // se abre un canal hacia el archivo para leer bytes
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // se encarga de capturar 70 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // lee paquetes de bytes hasta que llegue al final del archivo
            while (sbc.read(buffer) > 0) {
                // ubica el apuntador del buffer en la posición inicial
                buffer.rewind();
                // decodifica los bytes usando el juego de caracteres por defecto del sistema operativo
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                GrupoAnimal grupoAnimal = parseBufferGrupoAnimal(registro);
                grupoAnimales.add(grupoAnimal);
                //prepara al buffer para leer bytes del disco de nuevo
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return grupoAnimales;
    }

    @Override
    public Optional<GrupoAnimal> consultarPorCodigo(String codigo) {
        List<GrupoAnimal> codigoLista = mostrarGrupoAnimal();
        for (GrupoAnimal grupoAnimal : codigoLista){
            if(grupoAnimal.getCodigo().equals(codigo)){
                return Optional.of(grupoAnimal);
            }

        }
        return Optional.empty();
    }

    @Override
    public String codigoGeneral()   {

        String ultimoCodigo = "";
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 115 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimoCodigo = ultimoValorCodigo(registro);
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultimoCodigo;
    }

    private String ultimoValorCodigo(CharBuffer registro) {
        registro.position((LONGITUD_REGISTRO-LONGITUD_CODIGO));
        registro = registro.slice();
        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        return codigo;
    }

    @Override
    public String dietaGeneral() {

        String ultimoCodigo = "";
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 115 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimoCodigo = ultimoValorDieta(registro);
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultimoCodigo;
    }

    private String ultimoValorDieta(CharBuffer registro) {
        registro.position((LONGITUD_FECHA+LONGITUD_CANTIDAD+LONGITUD_TIPO));
        registro = registro.slice();
        String codigo = registro.subSequence(0, LONGITUD_DIETA).toString().trim();
        return codigo;
    }

    @Override
    public int cantidadGeneral() {
        int ultimaCantidad = 0 ;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 115 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimaCantidad = ultimoValorCantidad(registro);
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultimaCantidad;
    }

    private int ultimoValorCantidad(CharBuffer registro) {
        registro.position((LONGITUD_FECHA));
        registro = registro.slice();
        String codigo = registro.subSequence(0, LONGITUD_CANTIDAD).toString().trim();
        return Integer.parseInt(codigo);
    }

    private GrupoAnimal parseBufferGrupoAnimal(CharBuffer registro) {

        GrupoAnimal grupoAnimal = new GrupoAnimal();
        String fecha = registro.subSequence(0, LONGITUD_FECHA).toString().trim().replaceAll("/", "-");
        LocalDate fechaString = LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        grupoAnimal.setFechaRegistro(fechaString);
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();

        String cantidad = registro.subSequence(0, LONGITUD_CANTIDAD).toString().trim();
        int cantidadString = Integer.parseInt(cantidad);
        grupoAnimal.setCantidad(cantidadString);
        registro.position(LONGITUD_CANTIDAD);
        registro = registro.slice();

        String tipo = registro.subSequence(0, LONGITUD_TIPO).toString().trim();
        grupoAnimal.setTipo(tipo);
        registro.position(LONGITUD_TIPO);
        registro = registro.slice();

        String dieta = registro.subSequence(0, LONGITUD_DIETA).toString().trim();
        grupoAnimal.setDieta(dieta);
        registro.position(LONGITUD_DIETA);
        registro = registro.slice();

        String precio = registro.subSequence(0, LONGITUD_PRECIOPORKG).toString().trim();
        double precioString = Double.parseDouble(precio);
        grupoAnimal.setPrecioKg(precioString);
        registro.position(LONGITUD_PRECIOPORKG);
        registro = registro.slice();

        String ubicacion = registro.subSequence(0, LONGITUD_UBICACION).toString().trim();
        grupoAnimal.setUbicacion(ubicacion);
        registro.position(LONGITUD_UBICACION);
        registro = registro.slice();

        String codigo = registro.toString().trim();
        grupoAnimal.setCodigo(codigo);
        return grupoAnimal;
    }
}