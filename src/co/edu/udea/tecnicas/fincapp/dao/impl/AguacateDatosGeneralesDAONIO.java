package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.dao.AguacateDatosGeneralesDAO;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.WRITE;

public class AguacateDatosGeneralesDAONIO implements AguacateDatosGeneralesDAO {

    private final static String NOMBRE_ARCHIVO = "datosGeneralesAguacate";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static Integer LONGITUD_REGISTRO = 73;
    private final static Integer LONGITUD_PRECIO_KG = 7;
    private final static Integer LONGITUD_PRECIO_SEMILLA = 6;
    private final static Integer LONGITUD_UBICACION = 30;
    private final static Integer LONGITUD_CODIGO_GENERAL = 30;

    public AguacateDatosGeneralesDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarDatosGeneralesAguacate(DatosGeneralesCultivos datosGeneralesCultivos) {
        String registro = parseAguacateDatosString(datosGeneralesCultivos);
        byte[] datoRegistro = registro.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        try (FileChannel fc = (FileChannel.open(ARCHIVO, WRITE))) {
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<DatosGeneralesCultivos> consultarDatosGeneralesAguacates() {
        List<DatosGeneralesCultivos> datosGenerales = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 12 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            //ubica el apuntador en la posicon inicial
            while (sbc.read(buffer)>0){
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                DatosGeneralesCultivos datosGeneralesCultivos = parseBufferDatosGenerales(registro);
                datosGenerales.add(datosGeneralesCultivos);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return datosGenerales;
    }

    @Override
    public float consultarPrecioKg() {
        float precioKg = 0;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer)>0){
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                DatosGeneralesCultivos datosGeneralesCultivos = parseBufferDatosGenerales(registro);
                precioKg = datosGeneralesCultivos.getPrecioKg();
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return precioKg;
    }

    private DatosGeneralesCultivos parseBufferDatosGenerales(CharBuffer registro) {

        float precioKg = Float.parseFloat(registro.subSequence(0, LONGITUD_PRECIO_KG).toString().trim());
        registro.position(LONGITUD_PRECIO_KG);
        registro = registro.slice();

        float precioSemilla = Float.parseFloat(registro.subSequence(0, LONGITUD_PRECIO_SEMILLA).toString().trim());
        registro.position(LONGITUD_PRECIO_SEMILLA);
        registro = registro.slice();

        String ubicacion = registro.subSequence(0, LONGITUD_UBICACION).toString().trim();
        registro.position(LONGITUD_UBICACION);
        registro = registro.slice();

        String codigoGeneral = registro.subSequence(0, LONGITUD_CODIGO_GENERAL).toString().trim();
        registro.position(LONGITUD_CODIGO_GENERAL);
        registro = registro.slice();

        DatosGeneralesCultivos datosGeneralesCultivos = new DatosGeneralesCultivos(precioKg,precioSemilla,ubicacion,codigoGeneral);

        return datosGeneralesCultivos;

    }

    private String parseAguacateDatosString(DatosGeneralesCultivos datosGeneralesCultivos) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampos(Float.toString(datosGeneralesCultivos.getPrecioKg()), LONGITUD_PRECIO_KG))
                .append(completarCampos(Float.toString(datosGeneralesCultivos.getPrecioSemilla()), LONGITUD_PRECIO_SEMILLA))
                .append(completarCampos(datosGeneralesCultivos.getUbicacion(),LONGITUD_UBICACION))
                .append(completarCampos(datosGeneralesCultivos.getCodigoGeneral(),LONGITUD_CODIGO_GENERAL));
        return sb.toString();
    }

    private String completarCampos(String dato, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", dato);
    }


}
