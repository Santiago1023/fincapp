package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.model.Tomate;

import java.util.List;

public interface TomateDAO {
    void registrarTomate(Tomate tomate);
    List<Tomate> consultarTomates();
    List<Tomate> consultarTomatesDisponibles();
    List<Tomate> consultarTomatesVendidos();
    void venderTomate(int cantidadVenta) throws NoHayCantidadException;
    List<String> tiposFertilizantesRegistradosTmt();
    List<String> tiposAbonosRegistradosTmt();
    List<String> tiposInsecticidasRegistradosTmt();
    int codigoUltimoTomateRegistrado();
}
