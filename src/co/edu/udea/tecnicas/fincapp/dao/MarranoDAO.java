package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.Marrano;

import java.util.List;
import java.util.Optional;

public interface MarranoDAO {

    void registrarMarrano(Marrano marrano);

    List<Marrano> consultarMarranos();

    int verificarCantidad(String codigo);
}
