package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;

import java.time.LocalDate;
import java.util.List;

public interface AguacateDAO {

    void registrarAguacate(Aguacate aguacate);
    List<Aguacate> consultarAguacates();
    List<Aguacate> consultarAguacatesDisponibles();
    List<Aguacate> consultarAguacatesVendidos();
    void venderAguacates(int cantidadVentas) throws NoHayCantidadException;
    List<String> tiposFertilizantesRegistradosAgt();
    List<String> tiposAbonosRegistradosAgt();
    List<String> tiposInsecticidasRegistradosAgt();
    int codigoUltimoAguacateRegistrado();
}
