package co.edu.udea.tecnicas.fincapp.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.time.LocalDate;

public class vistaPrincipalController {

    @FXML
    private BorderPane contenedorVistaPpal;


    public void btnRegistrarAguacate_action() throws IOException {
        Parent ventanaRegistrarAgt = FXMLLoader.load(getClass().getResource("../view/registrar-aguacate.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaRegistrarAgt);
    }

    public void btnRegistrarTomate_action() throws IOException {
        Parent ventanaRegistrarTmt = FXMLLoader.load(getClass().getResource("../view/registrar-tomate.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaRegistrarTmt);
    }

    public void btnRegistrarMarrano_action() throws IOException {

        Parent ventanaRegistrarMarrano = FXMLLoader.load(getClass().getResource("../view/registrar-marrano.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaRegistrarMarrano);
    }

    public void btnRegistrarGallina_action() throws IOException {
        Parent ventanaRegistrarGallina = FXMLLoader.load(getClass().getResource("../view/registrar-gallina.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaRegistrarGallina);
    }

    public void btnCambiarPreciosAgt_action() throws IOException {
        Parent ventanaCambioPreciosAgt = FXMLLoader.load(getClass().getResource("../view/datos-generales-aguacates.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaCambioPreciosAgt);
    }

    public void btnCambiarPreciosTmt_action() throws IOException {
        Parent ventanaCambioPreciosTmt = FXMLLoader.load(getClass().getResource("../view/datos-generales-tomates.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaCambioPreciosTmt);
    }

    public void btnMostrarAguacates_action() throws IOException {
        Parent ventanaAguacatesRegistrados = FXMLLoader.load(getClass().getResource("../view/mostrar-aguacates.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaAguacatesRegistrados);
    }

    public void btnMostrarTomates_action() throws IOException {
        Parent ventanaTomatesRegistrados = FXMLLoader.load(getClass().getResource("../view/mostrar-tomates.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaTomatesRegistrados);
    }

    public void btnConsultarMarranos_action() throws IOException {
        Parent ventanaConsultarMarranos = FXMLLoader.load(getClass().getResource("../view/consultar-marranos.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaConsultarMarranos);
    }

    public void btnGenerarFacturaCultivo_action() throws IOException{
        Parent ventanaGenerarFactura = FXMLLoader.load(getClass().getResource("../view/registrar-venta-cultivo.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaGenerarFactura);
    }

    public void btnMostrarVenta_action() throws IOException{
        Parent ventanaMostrarVenta = FXMLLoader.load(getClass().getResource("../view/consultar-ventas.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaMostrarVenta);
    }

    public void btnRegistrarGrupoAnimal_action() throws IOException {
        Parent ventanaGrupoAnimal = FXMLLoader.load(getClass().getResource("../view/registrar-grupo-animal.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaGrupoAnimal);
        System.out.println(LocalDate.now().toString());
    }
    /*
    public void btnConsultarGallinas_action() throws IOException{
        Parent ventanaConsultarGallinas = FXMLLoader.load(getClass().getResource("../view/consultar-gallinas.fxml"));
        this.contenedorVistaPpal.setCenter(ventanaConsultarGallinas);
    }
     */
    public void btnSalir_action(){
        System.exit(0);
    }



}
