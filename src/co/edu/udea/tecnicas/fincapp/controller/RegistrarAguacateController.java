package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.AguacateDatosGeneralesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.AguacatesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class RegistrarAguacateController {

    @FXML
    private BorderPane contenedorRegistroAgt;
    @FXML
    private TextField txtCodigoLoteAgt;
    @FXML
    private TextField txtTipoFertAgt;
    @FXML
    private TextField txtTipoAbonoAgt;
    @FXML
    private TextField txtTipoInsecAgt;
    @FXML
    private TextField txtCantidadAgt;
    @FXML
    private TextField txtFecha;
    @FXML
    private SplitMenuButton smbFertilizanteAgt;
    @FXML
    private MenuItem mitOtroFertilizante;
    @FXML
    private SplitMenuButton smbAbonoAgt;
    @FXML
    private MenuItem mitOtroAbono;
    @FXML
    private SplitMenuButton smbInsecticidaAgt;
    @FXML
    private MenuItem mitOtroInsecticida;

    private AguacatesBsn aguacatesBsn;
    private AguacateDatosGeneralesBsn aguacateDatosGeneralesBsn;
    private List<DatosGeneralesCultivos> datosGeneralesCultivosList;
    private List<String> tiposDeFertilizante;
    private List<String> tiposDeAbono;
    private List<String> tiposDeInsecticidas;
    private String codigoGeneral;
    private int ultimoCodigo;
    private final String estado = "INGRESADO";

    @FXML
    public void initialize() throws IOException {
        refrescarCodigoFecha();
        refrescarSpliMenu();
        txtCantidadAgt.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=3){
                return change;
            }
            return null;
        }));
    }

    //Conexiones con el bsn del aguacate y de los datos generales del aguacate
    public RegistrarAguacateController() {
        this.aguacatesBsn = new AguacatesBsn();
        this.aguacateDatosGeneralesBsn = new AguacateDatosGeneralesBsn();
    }

    public void btnGuardarAgt_action() throws IOException {

        //Se toman los valores ingresados por el usuario
        String tipoFertIngresadoAgt = txtTipoFertAgt.getText().trim();
        String tipoAbonoIngresadoAgt = txtTipoAbonoAgt.getText().trim();
        String tipoInsecIngresadoAgt = txtTipoInsecAgt.getText().trim();
        String cantidadAgtIngresado = txtCantidadAgt.getText().trim();


        //Validacion de que los datos tengan un valor o sean del tipo requerido
        if (tipoFertIngresadoAgt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", "El tipo de fertilizante es requerido para registrar un aguacate");
            txtTipoFertAgt.requestFocus();
            return;
        }

        if (tipoAbonoIngresadoAgt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", "El tipo de abono es requerido para registrar un aguacate");
            txtTipoAbonoAgt.requestFocus();
            return;
        }

        if (tipoInsecIngresadoAgt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", "El tipo de insecticida es requerido para registrar un aguacate");
            txtTipoInsecAgt.requestFocus();
            return;
        }

        try {
            if (Integer.parseInt(cantidadAgtIngresado)<0){
                mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", "La cantidad de aguacates registrados debe de ser un numero positivo y es necesaria para completar el registro");
                txtCantidadAgt.requestFocus();
                txtCantidadAgt.clear();
                return;
            }
        } catch (NumberFormatException exception) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", "La cantidad de aguacates registrados es necesaria para completar el registro");
            txtCantidadAgt.requestFocus();
            txtCantidadAgt.clear();
            return;
        }

        //Registra la cantidad de aguacates dada
        for (int i = 1; i <= Integer.parseInt(cantidadAgtIngresado); i++) {
            StringBuilder codigoLoteAgt = new StringBuilder();
            ultimoCodigo++;
            codigoLoteAgt.append(codigoGeneral)
                    .append(ultimoCodigo);
            try {
                this.aguacatesBsn.registrarAguacate(codigoLoteAgt.toString(), tipoFertIngresadoAgt, tipoAbonoIngresadoAgt, tipoInsecIngresadoAgt, LocalDate.now(), estado);
            } catch (DatosIncorrectosException datosIncorrectosException){
                mostrarMensaje(Alert.AlertType.ERROR, "Registro Aguacate", "Error en los datos", datosIncorrectosException.getMessage());
                break;
            }
        }
        limpiarCampos();
        refrescarCodigoFecha();
        refrescarSpliMenu();
    }

    //Busca la fuente del boton con el tipo de dato que lo invoco y pone ese texto en el lugar donde el usuario pondria manualmente el dato y bloquea la edicion de este
    public void btnAgregarOpcionFertilizante_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoFertAgt.setText(menuInvocador.getText());
        txtTipoFertAgt.setVisible(true);
        txtTipoFertAgt.setEditable(false);
    }

    public void btnAgregarOpcionAbono_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoAbonoAgt.setText(menuInvocador.getText());
        txtTipoAbonoAgt.setVisible(true);
        txtTipoAbonoAgt.setEditable(false);
    }

    public void btnAgregarOpcionInsecticida_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoInsecAgt.setText(menuInvocador.getText());
        txtTipoInsecAgt.setVisible(true);
        txtTipoInsecAgt.setEditable(false);
    }

    //Da la opcion de volver a activar los campos de textos para agregar un dato nuevo
    public void btnOtroFertilizante_action() {
        txtTipoFertAgt.clear();
        txtTipoFertAgt.setEditable(true);
        txtTipoFertAgt.setVisible(true);
        txtTipoFertAgt.requestFocus();
    }

    public void btnOtroAbono_action() {
        txtTipoAbonoAgt.clear();
        txtTipoAbonoAgt.setEditable(true);
        txtTipoAbonoAgt.setVisible(true);
        txtTipoAbonoAgt.requestFocus();
    }

    public void btnOtroInsecticida_action() {
        txtTipoInsecAgt.clear();
        txtTipoInsecAgt.setEditable(true);
        txtTipoInsecAgt.setVisible(true);
        txtTipoInsecAgt.requestFocus();
    }

    private void limpiarCampos() {
        this.txtTipoInsecAgt.clear();
        this.txtTipoFertAgt.clear();
        this.txtTipoAbonoAgt.clear();
        this.txtCantidadAgt.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    //Actualiza el codigo y la fecha
    private void refrescarCodigoFecha() throws IOException {
        //Se visualiza en la vista la fecha del registro
        this.txtFecha.setText(LocalDate.now().toString());
        //Se intentan conseguir los datos generales de los aguacates y se muestra el codigo general y el codigo del ultimo registro o el primer registro
        try {
            datosGeneralesCultivosList = this.aguacateDatosGeneralesBsn.consultarDatosGeneralesAguacates();
            codigoGeneral = datosGeneralesCultivosList.get(0).getCodigoGeneral();
            ultimoCodigo = this.aguacatesBsn.codigoUltimoAguacateRegistrado();
            int siguienteCodigo = ultimoCodigo + 1;
            txtCodigoLoteAgt.setText(codigoGeneral + siguienteCodigo);
        } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro aguacates", "Error en datos generales", datosGeneralesRequeridosException.getMessage());
            Parent ventanaDatosGeneralesAgt = FXMLLoader.load(getClass().getResource("../view/datos-generales-aguacates.fxml"));
            this.contenedorRegistroAgt.setCenter(ventanaDatosGeneralesAgt);
        }
    }

    //Refresca el splitmenu con las opciones más recientes
    //Para no tener que volver a buscar todas las opciones un boolean
    private void refrescarSpliMenu(){
        //Se podran poner mas general?
        //Elimina las opciones que estaban antes para evitar duplicados
        for (MenuItem opcion : smbFertilizanteAgt.getItems()){
            smbFertilizanteAgt.getItems().retainAll(opcion);
        }
        for (MenuItem opcion : smbAbonoAgt.getItems()){
            smbAbonoAgt.getItems().retainAll(opcion);
        }
        for (MenuItem opcion : smbInsecticidaAgt.getItems()){
            smbInsecticidaAgt.getItems().retainAll(opcion);
        }

        //Se intentan buscar tipos de datos ya ingresados para añadirlos al splitmenu
        try {
            tiposDeFertilizante = this.aguacatesBsn.tiposFertilizantesRegistradosAgt();
            for (String tipoDeFertilizante : tiposDeFertilizante) {
                MenuItem nuevaOpcion = new MenuItem(tipoDeFertilizante);
                smbFertilizanteAgt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionFertilizante_action);
            }
            mitOtroFertilizante.setVisible(true);
            txtTipoFertAgt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroFertilizante.setVisible(false);
        }
        try {
            tiposDeAbono = this.aguacatesBsn.tiposAbonosRegistradosAgt();
            for (String tiposDeAbono : tiposDeAbono) {
                MenuItem nuevaOpcion = new MenuItem(tiposDeAbono);
                smbAbonoAgt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionAbono_action);
            }
            mitOtroAbono.setVisible(true);
            txtTipoAbonoAgt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroAbono.setVisible(false);
        }
        try {
            tiposDeInsecticidas = this.aguacatesBsn.tiposInsecticidasRegistradosAgt();
            for (String tiposDeInsecticida : tiposDeInsecticidas) {
                MenuItem nuevaOpcion = new MenuItem(tiposDeInsecticida);
                smbInsecticidaAgt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionInsecticida_action);
            }
            mitOtroInsecticida.setVisible(true);
            txtTipoInsecAgt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroInsecticida.setVisible(false);
        }
    }

}