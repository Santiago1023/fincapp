package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.GallinaBsn;
import co.edu.udea.tecnicas.fincapp.model.Gallina;
import co.edu.udea.tecnicas.fincapp.model.Marrano;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class registrarGallinaController {

    //Biding
    @FXML
    private TextField txtPesoGallina;
    @FXML
    private TextField txtAlimentoGallina;
    @FXML
    private TextField txtVacunaGallina;
    @FXML
    private TextField txtPrecioKgGallina;

    @FXML
    private TextField txtEstadoSaludGallina ;
    @FXML
    private TextField txtEdadGallina;
    @FXML
    private TextField txtLugarGallina;

    //conexion con el bsn
    private GallinaBsn gallinaBsn;

    public registrarGallinaController() {
        this.gallinaBsn = new GallinaBsn();
    }

    public void btnGuardar_action() {

        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String pesoIngresado = txtPesoGallina.getText().trim();
        String alimentoIngresado = txtAlimentoGallina.getText().trim();
        String vacunaIngresada = txtVacunaGallina.getText().trim();
        String precioKgIngresado = txtPrecioKgGallina.getText().trim();
        String estadoSaludIngresado = txtEstadoSaludGallina.getText().trim();
        String edadIngresado = txtEdadGallina.getText().trim();
        String lugarIngresado = txtLugarGallina.getText().trim();

        // se valida que el peso contenga un valor
        if (pesoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El peso es requerido");
            txtPesoGallina.requestFocus();
            return;
        }
        // se valida que el peso sea un número
        try {
            Integer.parseInt(pesoIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El peso debe ser un valor numérico");
            txtPesoGallina.requestFocus();
            txtPesoGallina.clear();
            return;
        }

        // se valida que alimento contenga un valor
        if (alimentoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El alimento es requerido");
            txtAlimentoGallina.requestFocus();
            return;
        }
        // se valida que la vacuna contenga un valor
        if (vacunaIngresada.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "La vacuna es requerida");
            txtVacunaGallina.requestFocus();
            return;
        }

        // se valida que el precio por kg contenga un valor
        if (precioKgIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El precio por kg es requerido");
            txtPrecioKgGallina.requestFocus();
            return;
        }
        // se valida que el precio por kg sea un número
        try {
            Integer.parseInt(precioKgIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El precio por kg debe ser un valor numérico");
            txtPrecioKgGallina.requestFocus();
            txtPrecioKgGallina.clear();
            return;
        }

        // se valida que el estado de salud contenga un valor
        if (estadoSaludIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El estado de salud es requerido");
            txtEstadoSaludGallina.requestFocus();
            return;
        }

        // se valida que la edad contenga un valor
        if (edadIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "La edad es requerida");
            txtEdadGallina.requestFocus();
            return;
        }
        // se valida que la edad sea un número
        try {
            Integer.parseInt(edadIngresado);
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "La edad debe ser un valor numérico");
            txtEdadGallina.requestFocus();
            txtEdadGallina.clear();
            return;
        }

        // se valida que el lugar contenga un valor
        if (lugarIngresado.isEmpty()) {

            mostrarMensaje(Alert.AlertType.ERROR, "Registro de gallina", "Resultado del registro", "El lugar es requerido");
            txtLugarGallina.requestFocus();
            return;
        }

        // creo la gallina
        Gallina gallina = new Gallina(pesoIngresado, alimentoIngresado, vacunaIngresada, precioKgIngresado,estadoSaludIngresado,edadIngresado,lugarIngresado);

        // tireselo al bsn
        //todo hacer que los métodos de almacenamiento retornen el objeto almacenado (copia) en consultarMarranosController

        this.gallinaBsn.registrarGallina(gallina);
        mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de gallina", "Resultado del registro", "La gallina ha sido registrado con éxito");
        limpiarCampos();

        /*consultarMarranosController marranosActualizado = new consultarMarranosController();
        marranosActualizado.refrescarTabla(marrano);*/


    }

    private void limpiarCampos() {
        this.txtPesoGallina.clear();
        this.txtAlimentoGallina.clear();
        this.txtVacunaGallina.clear();
        this.txtPrecioKgGallina.clear();
        this.txtEstadoSaludGallina.clear();
        this.txtEdadGallina.clear();
        this.txtLugarGallina.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {

        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
