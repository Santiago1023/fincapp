package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.GrupoAnimalBsn;
import co.edu.udea.tecnicas.fincapp.bsn.MarranoBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.FaltaRegistrarMarranosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.RegistroGeneralYaExisteException;
import co.edu.udea.tecnicas.fincapp.model.GrupoAnimal;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import java.io.IOException;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class registrarGrupoAnimalController {

    @FXML
    private BorderPane bpDatosGenerales;
    @FXML
    private TextField txtTipo;
    @FXML
    private TextField txtCantidad;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtUbicacion;
    @FXML
    private TextField txtDieta;
    @FXML
    private TextField txtPrecioKgCarne;
    @FXML
    private TextField txtFecha;
    @FXML
    private TableView<GrupoAnimal> tblDatosGenerales;
    //todo verificar bien como son los datos del segundo parametro para columnas, porque ponía los tipos que eran y me daba error
    @FXML
    private TableColumn<GrupoAnimal, String > clmFecha;
    @FXML
    private TableColumn<GrupoAnimal, String> clmCantidad;
    @FXML
    private TableColumn<GrupoAnimal, String > clmTipo;
    @FXML
    private TableColumn<GrupoAnimal, String > clmDieta;
    @FXML
    private TableColumn<GrupoAnimal, String > clmPrecio;
    @FXML
    private TableColumn<GrupoAnimal, String > clmUbicacion;
    @FXML
    private TableColumn<GrupoAnimal, String > clmCodigo;

    private int cont;

    private MarranoBsn marranoBsn;

    //todo hacer que cuando le de en el boton guardar, se muestre un mensaje que diga que tiene que ir a registrar marrano por marrano , (podría ser que se bloquera la pagina)

    private GrupoAnimalBsn grupoAnimalBsn;

    private registrarMarranoController conexion;

    public registrarGrupoAnimalController(){
        this.grupoAnimalBsn = new GrupoAnimalBsn();
        this.marranoBsn = new MarranoBsn();
    }


    //todo que no se abra la ventana hasta que no estén todos los marranos registrados , una forma podría ser poner un condicional que evalue si ya registramos todos los marranos (si es true trabaje normal, y si es falso que vuelva setEditable(false) o invisible)
    @FXML
    public void initialize() throws IOException {
        refrescarCantidad();
        refrescarCodigoFecha();
        clmFecha.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFechaRegistro().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        clmCantidad.setCellValueFactory(cellData -> new SimpleStringProperty(Integer.toString(cellData.getValue().getCantidad())));
        clmTipo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getTipo()));
        clmDieta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDieta()));
        clmPrecio.setCellValueFactory(cellData -> new SimpleStringProperty(Double.toString(cellData.getValue().getPrecioKg())));
        clmUbicacion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUbicacion()));
        clmCodigo.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
        refrescarTabla();

        txtPrecioKgCarne.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=4){
                return change;
            }
            return null;
        }));

        txtCantidad.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=2){
                return change;
            }
            return null;
        }));
    }

    public void btnGuardar_action() throws IOException {

        // se extraen los datos del view
        String tipoIngresado = txtTipo.getText().trim();
        String cantidadIngresado = txtCantidad.getText().trim();
        String codigoIngresado = txtCodigo.getText().trim();
        String ubicacionIngresado = txtUbicacion.getText().trim();
        String dietaIngresado = txtDieta.getText().trim();
        String precioIngresado = txtPrecioKgCarne.getText().trim();

        if (tipoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El tipo de animal es requerido");
            txtTipo.requestFocus();
            return;
        }
        if (cantidadIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "La cantidad es requerida");
            txtCantidad.requestFocus();
            return;
        }
        try {
            if (Integer.parseInt(cantidadIngresado) <= 0) {
                mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "La cantidad debe ser un numero mayor que 0");
                txtCantidad.requestFocus();
                txtCantidad.clear();
                return;
            }
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El peso debe ser un valor numérico");
            txtCantidad.requestFocus();
            txtCantidad.clear();
            return;
        }
        if (codigoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El codigo es requerido");
            txtCodigo.requestFocus();
            return;
        }
        if (ubicacionIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "La ubicacion es requerida");
            txtUbicacion.requestFocus();
            return;
        }
        if (dietaIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "La dieta es requerida");
            txtDieta.requestFocus();
            return;
        }
        if (precioIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El precio por kg es requerido");
            txtPrecioKgCarne.requestFocus();
            return;
        }
        try {
            if (Integer.parseInt(precioIngresado) <= 0) {
                mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El precio por kg debe ser un numero mayor que 0");
                txtPrecioKgCarne.requestFocus();
                txtPrecioKgCarne.clear();
                return;
            }
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro animal", "Resultado del registro", "El precio por kg debe ser un valor numérico");
            txtPrecioKgCarne.clear();
            txtPrecioKgCarne.requestFocus();
            return;
        }

        double precioConvertido = Double.parseDouble(precioIngresado);
        int cantidadConvertida = Integer.parseInt(cantidadIngresado);
        LocalDate fechaConvertida = LocalDate.parse(this.txtFecha.getText());
        GrupoAnimal grupoAnimal = new GrupoAnimal(tipoIngresado, cantidadConvertida, codigoIngresado, ubicacionIngresado, dietaIngresado, precioConvertido, fechaConvertida);

        cont = cantidadConvertida;

        try {
            this.grupoAnimalBsn.registrarGrupoAnimal(grupoAnimal);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro animal", "Resultado del registro", "El animal ha sido registrado con exito");
            limpiarCampos();
            GrupoAnimal grupoAnimalClone = grupoAnimal.clone();
            tblDatosGenerales.getItems().add(grupoAnimalClone);
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro animal", "Recomendacion", "Se recomienda que registre los" + " "+ cont + " " + tipoIngresado +"s");

        }
        catch (RegistroGeneralYaExisteException e){
            mostrarMensaje(Alert.AlertType.ERROR, "Registro grupo animal", "Resultado del registro", e.getMessage());
            txtCodigo.clear();
            txtCodigo.requestFocus();
        }
         catch (CloneNotSupportedException cnse){
            this.refrescarTabla();
        }

        refrescarCantidad();

    }
    private void limpiarCampos() throws IOException {
        this.txtDieta.clear();
        this.txtCodigo.clear();
        this.txtUbicacion.clear();
        this.txtPrecioKgCarne.clear();
        this.txtCantidad.clear();
        this.txtUbicacion.clear();
        this.txtFecha.clear();
        this.txtTipo.clear();
        refrescarCodigoFecha();

    }
    private void refrescarCodigoFecha() throws IOException {
        //Se visualiza en la vista la fecha del registro
        this.txtFecha.setText(LocalDate.now().toString());
    }
    public void btnMarrano_action(){
        txtTipo.clear();
        txtTipo.setEditable(false);
        txtTipo.setVisible(true);
        txtTipo.setText("Marrano");
    }
    public void btnGallina_action(){
        txtTipo.clear();
        txtTipo.setEditable(false);
        txtTipo.setVisible(true);
        txtTipo.setText("Gallina");
    }
    public void btnConcentrado_action(){
        txtDieta.clear();
        txtDieta.setEditable(false);
        txtDieta.setVisible(true);
        txtDieta.setText("Concentrado");
    }
    public void btnAguamasa_action(){
        txtDieta.clear();
        txtDieta.setEditable(false);
        txtDieta.setVisible(true);
        txtDieta.setText("Aguamasa");
    }
    public void btnAmbos_action(){
        txtDieta.clear();
        txtDieta.setEditable(false);
        txtDieta.setVisible(true);
        txtDieta.setText("Ambos");
    }
    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
    private void refrescarTabla(){
        List<GrupoAnimal> grupoAnimalList = grupoAnimalBsn.mostrarGrupoAnimal();
        ObservableList<GrupoAnimal> grupoAnimalObservableList = FXCollections.observableList(grupoAnimalList);
        tblDatosGenerales.setItems(grupoAnimalObservableList);
    }

    private void refrescarCantidad() throws IOException{
        try {
            marranoBsn.verificarCantidad();
        }
        catch (FaltaRegistrarMarranosException e) {
            mostrarMensaje(Alert.AlertType.WARNING, "Recomendacion","Faltan marranos por registrar",e.getMessage() );
            Parent ventanaRegistrarMarrano = FXMLLoader.load(getClass().getResource("../view/registrar-marrano.fxml"));
            this.bpDatosGenerales.setCenter(ventanaRegistrarMarrano);

        }
    }
}
