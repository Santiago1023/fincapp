package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.GallinaBsn;
import co.edu.udea.tecnicas.fincapp.bsn.MarranoBsn;
import co.edu.udea.tecnicas.fincapp.model.Gallina;
import co.edu.udea.tecnicas.fincapp.model.Marrano;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;

public class consultarGallinasController {
    @FXML
    private TableView<Gallina> tblGallinas;
    @FXML
    private TableColumn<Gallina, String> clmPeso;
    @FXML
    private TableColumn<Gallina, String> clmAlimento;
    @FXML
    private TableColumn<Gallina, String> clmVacuna;
    @FXML
    private TableColumn<Gallina, String> clmPrecioKg;
    @FXML
    private TableColumn<Gallina, String> clmEstadoSalud;
    @FXML
    private TableColumn<Gallina, String> clmEdad;
    @FXML
    private TableColumn<Gallina, String> clmLugar;

    //conexion con el negocio
    private GallinaBsn gallinaBsn;

    public consultarGallinasController(){
        this.gallinaBsn = new GallinaBsn();
    }

    @FXML
    public void initialize(){
        clmPeso.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPeso()));
        clmAlimento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAlimento()));
        clmVacuna.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getVacuna()));
        clmPrecioKg.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getPrecioKg()));
        clmEstadoSalud.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstadoSalud()));
        clmEdad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEdad()));
        clmLugar.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getLugar()));

        List<Gallina> gallinasList = gallinaBsn.consultarGallinas();
        ObservableList<Gallina> gallinasListObservable = FXCollections.observableList(gallinasList);
        tblGallinas.setItems(gallinasListObservable);

    }
}
