package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.VentaBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;
import co.edu.udea.tecnicas.fincapp.model.Venta;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.util.StringConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class ConsultarVentasController {
    @FXML
    private TableView<Venta> tblVentas;
    @FXML
    private TableColumn<Venta, String> cmlCodigoFactura;
    @FXML
    private TableColumn<Venta, String> cmlProducto;
    @FXML
    private TableColumn<Venta, String> cmlCantidad;
    @FXML
    private TableColumn<Venta, String> cmlPrecioVenta;
    @FXML
    private TableColumn<Venta, LocalDate> cmlFechaVenta;
    @FXML
    private Label lblVentas;
    @FXML
    private GridPane gpVentas;
    @FXML
    private Label lblDiasMes;
    @FXML
    private TextField txtDiasMes;
    @FXML
    private TextField txtTotalVentas;
    @FXML
    private Button btnCalcular;
    @FXML
    private RadioButton rbVentasFechas;
    @FXML
    private RadioButton rbVentasDias;
    @FXML
    private RadioButton rbVentasMes;
    @FXML
    private RadioButton rbVentasTotales;
    @FXML
    private DatePicker dpFechaInicio;
    @FXML
    private DatePicker dpFechaFin;

    private VentaBsn ventaBsn;

    public ConsultarVentasController() {
        this.ventaBsn = new VentaBsn();
    }

    @FXML
    public void initialize() {
        cmlCodigoFactura.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoFactura()));
        cmlProducto.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getProducto()));
        cmlCantidad.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getCantidad())));
        cmlPrecioVenta.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPrecioVenta())));
        cmlFechaVenta.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getFecha()));
        formatosFecha();
        refrescar();

    }

    public void btnCalcular_action() {

        if (rbVentasFechas.isSelected()){
            String fechaInicioIngresada = dpFechaInicio.getEditor().getText(); //txtFechaInicio.getText().trim();
            String fechaFinIngresada = dpFechaFin.getEditor().getText(); //txtFechaFin.getText().trim();

            if (fechaInicioIngresada.isEmpty()){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "La fecha de inicio es requerida");
                dpFechaInicio.getEditor().clear();
                dpFechaInicio.getEditor().requestFocus();
                return;
            }

            if (fechaFinIngresada.isEmpty()){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "La fecha de fin es requerida");
                dpFechaFin.getEditor().clear();
                dpFechaFin.getEditor().requestFocus();
                return;
            }
            //Pense que no se necesitaba pero si, porque la persona puede escribir la fecha en vez de escogerla, pondria editable false pero es mejor asi por si es una fecha maluca de buscar
            //Mejor pongo editable false :v puedo escribir febrero 31, esta validacion la dejo por si algo
            if (!fechaInicioIngresada.matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "La fecha no tiene el formato requerido");
                dpFechaInicio.getEditor().clear();
                dpFechaInicio.getEditor().requestFocus();
                return;
            }
            if (!fechaFinIngresada.matches("^\\d{4}\\-(0[1-9]|1[012])\\-(0[1-9]|[12][0-9]|3[01])$")){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "La fecha no tiene el formato requerido");
                dpFechaFin.getEditor().clear();
                dpFechaFin.getEditor().requestFocus();
                return;
            }


            LocalDate fechaInicio = LocalDate.parse(fechaInicioIngresada);
            LocalDate fechaFin = LocalDate.parse(fechaFinIngresada);
            try {
                List<Venta> ventaList = ventaBsn.consultarVentasEntreFechas(fechaInicio,fechaFin);
                ObservableList<Venta> ventaObservableList = FXCollections.observableList(ventaList);
                tblVentas.setItems(ventaObservableList);
                long valor = calcularTotal(ventaList);
                txtTotalVentas.setText(Long.toString(valor));
                txtTotalVentas.setVisible(true);
                lblVentas.setVisible(true);
            } catch (NoHayDatosException noHayDatosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", noHayDatosException.getMessage());
            }
        }

        if (rbVentasDias.isSelected()) {
            String diaMesIngresadoString = txtDiasMes.getText().trim();

            if (diaMesIngresadoString.isEmpty()){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "El numero de dias es requerido");
                txtDiasMes.clear();
                txtDiasMes.requestFocus();
                return;
            }

            int diaMesIngresado = Integer.parseInt(txtDiasMes.getText().trim());

            try {
                List<Venta> ventaList = ventaBsn.consultarVentasDias(diaMesIngresado);
                ObservableList<Venta> ventaObservableList = FXCollections.observableList(ventaList);
                tblVentas.setItems(ventaObservableList);
                long valor = calcularTotal(ventaList);
                txtTotalVentas.setText(Long.toString(valor));
                txtTotalVentas.setVisible(true);
                lblVentas.setVisible(true);
            } catch (NoHayDatosException noHayDatosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", noHayDatosException.getMessage());
            }
        }

        if (rbVentasMes.isSelected()){

            String diaMesIngresadoString = txtDiasMes.getText().trim();

            if (diaMesIngresadoString.isEmpty()){
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", "El numero del mes es requerido");
                txtDiasMes.clear();
                txtDiasMes.requestFocus();
                return;
            }

            int diaMesIngresado = Integer.parseInt(txtDiasMes.getText().trim());

            try {
                List<Venta> ventaList = ventaBsn.consultarVentasMes(diaMesIngresado);
                ObservableList<Venta> ventaObservableList = FXCollections.observableList(ventaList);
                tblVentas.setItems(ventaObservableList);
                long valor = calcularTotal(ventaList);
                txtTotalVentas.setText(Long.toString(valor));
                txtTotalVentas.setVisible(true);
                lblVentas.setVisible(true);
            } catch (NoHayDatosException noHayDatosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", noHayDatosException.getMessage());
            }
        }

        if (rbVentasTotales.isSelected()){
            try {
                List<Venta> ventaList = ventaBsn.consultarVentas();
                ObservableList<Venta> ventaObservableList = FXCollections.observableList(ventaList);
                tblVentas.setItems(ventaObservableList);
                long valor = calcularTotal(ventaList);
                txtTotalVentas.setText(Long.toString(valor));
                txtTotalVentas.setVisible(true);
                lblVentas.setVisible(true);
            } catch (NoHayDatosException noHayDatosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", noHayDatosException.getMessage());
            }
        }

        rbVentasFechas.setSelected(false);
        rbVentasTotales.setSelected(false);
        rbVentasMes.setSelected(false);
        rbVentasDias.setSelected(false);
        btnCalcular.setVisible(false);
    }

    public void rbVentasFechas_action() {
        btnCalcular.setVisible(true);
        rbVentasMes.setSelected(false);
        rbVentasTotales.setSelected(false);
        rbVentasDias.setSelected(false);
        dpFechaInicio.setVisible(true);
        dpFechaFin.setVisible(true);
        txtDiasMes.setVisible(false);
        lblDiasMes.setVisible(false);
        lblVentas.setVisible(false);
        txtTotalVentas.setVisible(false);
        limpiarCampos();
    }

    public void rbVentasDias_action() {
        btnCalcular.setVisible(true);
        rbVentasMes.setSelected(false);
        rbVentasTotales.setSelected(false);
        rbVentasFechas.setSelected(false);
        dpFechaInicio.setVisible(false);
        dpFechaFin.setVisible(false);
        txtDiasMes.setVisible(true);
        lblVentas.setVisible(false);
        txtTotalVentas.setVisible(false);
        lblDiasMes.setVisible(true);
        lblDiasMes.setText("DIAS");
        limpiarCampos();
        txtDiasMes.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=3){
                return change;
            }
            return null;
        }));
    }

    public void rbVentasMes_action() {
        btnCalcular.setVisible(true);
        rbVentasDias.setSelected(false);
        rbVentasTotales.setSelected(false);
        rbVentasFechas.setSelected(false);
        dpFechaInicio.setVisible(false);
        dpFechaFin.setVisible(false);
        txtDiasMes.setVisible(true);
        lblVentas.setVisible(false);
        txtTotalVentas.setVisible(false);
        lblDiasMes.setVisible(true);
        lblDiasMes.setText("MES");
        limpiarCampos();
        txtDiasMes.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("(^(1[0-2]|[1-9])$*)?") && change.getControlNewText().length()<=2){
                return change;
            }
            return null;
        }));
    }

    public void rbVentasTotales_action() {
        btnCalcular.setVisible(true);
        rbVentasMes.setSelected(false);
        rbVentasDias.setSelected(false);
        rbVentasFechas.setSelected(false);
        dpFechaInicio.setVisible(false);
        dpFechaFin.setVisible(false);
        txtDiasMes.setVisible(false);
        lblDiasMes.setVisible(false);
        lblVentas.setVisible(false);
        txtTotalVentas.setVisible(false);
        limpiarCampos();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    private void refrescar(){
        try {
            List<Venta> ventaList = ventaBsn.consultarVentas();
            ObservableList<Venta> ventaObservableList = FXCollections.observableList(ventaList);
            tblVentas.setItems(ventaObservableList);
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Ventas", "Consultar ventas", noHayDatosException.getMessage());
        }
    }

    private void limpiarCampos(){
        txtDiasMes.clear();
        dpFechaFin.getEditor().clear();
        dpFechaInicio.getEditor().clear();
        txtTotalVentas.clear();
    }

    private long calcularTotal(List<Venta> ventaList){
        long valor =0;
        for (Venta venta:ventaList) {
            valor+=venta.getPrecioVenta();
        }
        return valor;
    }

   //Referencia https://stackoverflow.com/questions/26831978/javafx-datepicker-getvalue-in-a-specific-format
    private void formatosFecha(){
        dpFechaFin.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);
            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
        dpFechaInicio.setConverter(new StringConverter<LocalDate>() {
            String pattern = "yyyy-MM-dd";
            DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

            @Override public String toString(LocalDate date) {
                if (date != null) {
                    return dateFormatter.format(date);
                } else {
                    return "";
                }
            }

            @Override public LocalDate fromString(String string) {
                if (string != null && !string.isEmpty()) {
                    return LocalDate.parse(string, dateFormatter);
                } else {
                    return null;
                }
            }
        });
    }

}
