package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.GrupoAnimalBsn;
import co.edu.udea.tecnicas.fincapp.bsn.MarranoBsn;
import co.edu.udea.tecnicas.fincapp.model.Marrano;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.time.format.DateTimeFormatter;
import java.util.List;

public class consultarMarranosController {

    //Biding atributos para manejar componenetes desde codigo

    //todo revisar si en el segundo parametro necesita el tipo de dato correcto o String
    @FXML
    private TableView<Marrano> tblMarranos;
    @FXML
    private TableColumn<Marrano, String> clmCodigoRegistro;
    @FXML
    private TableColumn<Marrano, String> clmCodigoAnimal;
    @FXML
    private TableColumn<Marrano, String> clmDieta;
    @FXML
    private TableColumn<Marrano, String> clmPeso;
    @FXML
    private TableColumn<Marrano, String> clmEstadoSalud;
    @FXML
    private TableColumn<Marrano, String> clmFechaNacimiento;


    //conexion con el negocio
    private MarranoBsn marranoBsn;



    private GrupoAnimalBsn grupoAnimalBsn;

    // comunicar el marranoBSN y grupoAnimalBSN con los negocios respectivos


    public consultarMarranosController(){
        this.marranoBsn = new MarranoBsn();
        this.grupoAnimalBsn = new GrupoAnimalBsn();
    }

    @FXML
    public void initialize() {
        //todo poner el codigoRegistro de la clase de datos generales
        clmCodigoAnimal.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigo()));
        clmDieta.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDietaGeneral()));
        clmPeso.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPeso())));
        clmEstadoSalud.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstadoSalud()));
        clmFechaNacimiento.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFechaNacimiento().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))));
        clmCodigoRegistro.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoGeneral()));

        List<Marrano> marranosList = marranoBsn.consultarMarranos();
        ObservableList<Marrano> marranosListObservable = FXCollections.observableList(marranosList);
        tblMarranos.setItems(marranosListObservable);


    }

   private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {

       Alert alert = new Alert(tipo);
       alert.setTitle(titulo);
       alert.setHeaderText(encabezado);
       alert.setContentText(mensaje);
       alert.showAndWait();
   }


}
