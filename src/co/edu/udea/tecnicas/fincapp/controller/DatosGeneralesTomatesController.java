package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.TomateDatosGeneralesBSN;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;
import co.edu.udea.tecnicas.fincapp.model.Tomate;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Optional;

public class DatosGeneralesTomatesController {
    @FXML
    private GridPane gpDatos;
    @FXML
    private TextField txtPrecioKgTmt;
    @FXML
    private TextField txtPrecioSemillaTmt;
    @FXML
    private TextField txtCodigoGeneralTmt;
    @FXML
    private TextField txtUbicacionTmt;
    @FXML
    private ButtonBar btnbarCambios;
    @FXML
    private Button btnGuardar;
    @FXML
    private TableView<DatosGeneralesCultivos> tblDatosTmt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlPrecioKgTmt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlPrecioSemillaTmt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlCodigoTmt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlUbicacionTmt;
    @FXML
    private Label lblCambios;

    private TomateDatosGeneralesBSN tomateDatosGeneralesBSN;

    public DatosGeneralesTomatesController() {
        this.tomateDatosGeneralesBSN = new TomateDatosGeneralesBSN();
    }

    @FXML
    public void initialize() {
        refrescar();
    }

    public void btnGuardarPrecioTmt_action() {

        String precioKgIngresadoTmt = txtPrecioKgTmt.getText().trim();
        String precioSemillaIngresadoTmt = txtPrecioSemillaTmt.getText().trim();
        String ubicacionIngresadoTmt = txtUbicacionTmt.getText().trim();
        String codigoGeneralIngresadoTmt = txtCodigoGeneralTmt.getText().trim();

        try {
            Float.parseFloat(precioKgIngresadoTmt);
        } catch (NumberFormatException exception) {
            //exception.printStackTrace();
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales tomate", "Error en los datos", "El precio del kg es requerido y debe ser numerico");
            txtPrecioKgTmt.requestFocus();
            txtPrecioKgTmt.clear();
            return;
        }

        try {
            Float.parseFloat(precioSemillaIngresadoTmt);
        } catch (NumberFormatException exception) {
            //exception.printStackTrace();
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales tomate", "Error en los datos", "El precio de la semilla es requerido y debe ser numerico");
            txtPrecioSemillaTmt.requestFocus();
            txtPrecioSemillaTmt.clear();
            return;
        }

        if (codigoGeneralIngresadoTmt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales tomate", "Error en los datos", "El codigo general es requerido");
            txtCodigoGeneralTmt.requestFocus();
            return;
        }

        if (ubicacionIngresadoTmt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales tomate", "Error en los datos", "La ubicación es requerida");
            txtUbicacionTmt.requestFocus();
            return;
        }

        try {
            this.tomateDatosGeneralesBSN.registrarDatosGeneralesTomate(precioKgIngresadoTmt, precioSemillaIngresadoTmt, ubicacionIngresadoTmt, codigoGeneralIngresadoTmt);
        } catch (DatosIncorrectosException exception) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales tomate", "Error en los datos", exception.getMessage());
        }

        refrescar();
        limpiarCampos();
    }

    public void btnCambiarPrecios_action() {
        //Una exception podria ser no existe
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio precios", "Esta seguro que desea cambiar los precios generales de los tomates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = tomateDatosGeneralesBSN.consultarDatosGeneralesTomates();
                txtCodigoGeneralTmt.setText(datosGeneralesCultivosList.get(0).getCodigoGeneral());
                txtUbicacionTmt.setText(datosGeneralesCultivosList.get(0).getUbicacion());
                txtPrecioKgTmt.setEditable(true);
                txtPrecioSemillaTmt.setEditable(true);
                txtCodigoGeneralTmt.setEditable(false);
                txtUbicacionTmt.setEditable(false);
                btnGuardar.setVisible(true);
                btnbarCambios.setVisible(false);
                gpDatos.setVisible(true);
                lblCambios.setVisible(false);
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarCodigo_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio codigo general", "Esta seguro que desea cambiar el codigo general de los tomates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = tomateDatosGeneralesBSN.consultarDatosGeneralesTomates();
                txtPrecioKgTmt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioKg()));
                txtPrecioSemillaTmt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioSemilla()));
                txtUbicacionTmt.setText(datosGeneralesCultivosList.get(0).getUbicacion());
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
            txtPrecioKgTmt.setEditable(false);
            txtPrecioSemillaTmt.setEditable(false);
            txtCodigoGeneralTmt.setEditable(true);
            txtUbicacionTmt.setEditable(false);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarUbicacion_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio ubicacion", "Esta seguro que desea cambiar la ubicacion general de los tomates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = tomateDatosGeneralesBSN.consultarDatosGeneralesTomates();
                txtPrecioKgTmt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioKg()));
                txtPrecioSemillaTmt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioSemilla()));
                txtCodigoGeneralTmt.setText(datosGeneralesCultivosList.get(0).getCodigoGeneral());
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
            //Podria meter esto en un metodo
            txtPrecioKgTmt.setEditable(false);
            txtPrecioSemillaTmt.setEditable(false);
            txtCodigoGeneralTmt.setEditable(false);
            txtUbicacionTmt.setEditable(true);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarTodo_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambiar todo", "Esta seguro que desea cambiar todo los datos generales de los tomates")) {
            txtPrecioKgTmt.setEditable(true);
            txtPrecioSemillaTmt.setEditable(true);
            txtCodigoGeneralTmt.setEditable(true);
            txtUbicacionTmt.setEditable(true);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    private boolean mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        if (tipo == Alert.AlertType.CONFIRMATION) {
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                return true;
            } else {
                return false;
            }
        } else {
            alert.showAndWait();
        }
        return false;
    }

    private void refrescar() {
        try {
            cmlPrecioKgTmt.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPrecioKg())));
            cmlPrecioSemillaTmt.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPrecioSemilla())));
            cmlUbicacionTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUbicacion()));
            cmlCodigoTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoGeneral()));
            //Misma razon que en el aguacate
            /*
            txtPrecioKgTmt.setTextFormatter(new TextFormatter<>(change -> {
                if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 4) {
                    return change;
                }
                return null;
            }));
            txtPrecioSemillaTmt.setTextFormatter(new TextFormatter<>(change -> {
                if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 3) {
                    return change;
                }
                return null;
            }));
             */
            List<DatosGeneralesCultivos> datosGeneralesCultivosList = tomateDatosGeneralesBSN.consultarDatosGeneralesTomates();
            ObservableList<DatosGeneralesCultivos> datosGeneralesCultivosObservableList = FXCollections.observableList(datosGeneralesCultivosList);
            tblDatosTmt.setItems(datosGeneralesCultivosObservableList);

            if (datosGeneralesCultivosList.size() != 0) {
                lblCambios.setVisible(true);
                tblDatosTmt.setVisible(true);
                btnbarCambios.setVisible(true);
                btnGuardar.setVisible(false);
                gpDatos.setVisible(false);
            } else {
                gpDatos.setVisible(true);
                btnGuardar.setVisible(true);
            }
        } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
        }
    }

    private void limpiarCampos() {
        this.txtPrecioSemillaTmt.clear();
        this.txtPrecioKgTmt.clear();
        this.txtUbicacionTmt.clear();
        this.txtCodigoGeneralTmt.clear();
    }
}
