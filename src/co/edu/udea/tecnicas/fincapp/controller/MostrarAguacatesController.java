package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.AguacatesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.time.LocalDate;
import java.util.List;

public class MostrarAguacatesController {
    @FXML
    private Label lblTitulo;
    @FXML
    private TableView<Aguacate> tblAguacates;
    @FXML
    private TableColumn<Aguacate, String> cmlTipoAgt;
    @FXML
    private TableColumn<Aguacate, String> cmlFertilizanteAgt;
    @FXML
    private TableColumn<Aguacate, String> cmlAbonoAgt;
    @FXML
    private TableColumn<Aguacate, String> cmlInsecticidaAgt;
    @FXML
    private TableColumn<Aguacate, LocalDate> cmlFecha;
    @FXML
    private TableColumn<Aguacate, String> cmlEstado;

    private AguacatesBsn aguacatesBsn;

    public MostrarAguacatesController() {
        this.aguacatesBsn = new AguacatesBsn();
    }

    @FXML
    public void initialize() {
        cmlTipoAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoLote()));
        cmlFertilizanteAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFertilizante()));
        cmlAbonoAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAbono()));
        cmlInsecticidaAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getInsecticida()));
        cmlFecha.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getFecha()));
        cmlEstado.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getEstado()));
        btnMostrarTodos_action();
    }

    public void btnMostrarVendidos_action() {
        try {
            List<Aguacate> aguacateList = aguacatesBsn.consultarAguacatesVendidos();
            ObservableList<Aguacate> aguacateListObs = FXCollections.observableList(aguacateList);
            tblAguacates.setItems(aguacateListObs);
            lblTitulo.setText("AGUACATES VENDIDOS");
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Aguacates registrados", "No hay aguacates vendidos", noHayDatosException.getMessage());
        }
    }

    public void btnMostrarIngresados_action() {
        try {
            List<Aguacate> aguacateList = aguacatesBsn.consultarAguacatesIngresados();
            ObservableList<Aguacate> aguacateListObs = FXCollections.observableList(aguacateList);
            tblAguacates.setItems(aguacateListObs);
            lblTitulo.setText("AGUACATES INGRESADOS");
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Aguacates registrados", "No hay aguacates registrados", noHayDatosException.getMessage());
        }
    }

    public void btnMostrarTodos_action() {
        lblTitulo.setText("AGUACATES");
        try {
            List<Aguacate> aguacateList = aguacatesBsn.consultarAguacates();
            ObservableList<Aguacate> aguacateListObs = FXCollections.observableList(aguacateList);
            tblAguacates.setItems(aguacateListObs);
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Aguacates registrados", "No hay aguacates ingresados", noHayDatosException.getMessage());
        }
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

}
