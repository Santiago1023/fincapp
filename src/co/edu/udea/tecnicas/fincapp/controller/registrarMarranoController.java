package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.GrupoAnimalBsn;
import co.edu.udea.tecnicas.fincapp.bsn.MarranoBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.FaltaRegistrarMarranosException;
import co.edu.udea.tecnicas.fincapp.model.Marrano;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class registrarMarranoController {

    // para controlar los componentes de javafx desde codigo

    @FXML
    private BorderPane vistaRegistro;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtEstadoSalud;
    @FXML
    private SplitMenuButton smbDieta;
    @FXML
    private TextField txtPeso;
    @FXML
    private SplitMenuButton smbEstadoSalud ;
    @FXML
    private MenuItem mConcentrado;
    @FXML
    private MenuItem mAguamasa;
    @FXML
    private MenuItem mAmbos;
    @FXML
    private MenuItem mEnfermo;
    @FXML
    private MenuItem mSano;
    @FXML
    private DatePicker dpFecha;

    //conexion con el bsn
    private MarranoBsn marranoBsn;

    private GrupoAnimalBsn grupoAnimalBsn;

    private int contador;

    //creamos la instancia de bsn
    public registrarMarranoController() {
        this.marranoBsn = new MarranoBsn();
        this.grupoAnimalBsn = new GrupoAnimalBsn();
    }

    @FXML
    public void initialize () throws IOException {
        //mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de marranos","Conteo de registros", "Tiene por registrar" + " "+ contador+ " " +"marranos" );

        txtPeso.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=3){
                return change;
            }
            return null;
        }));

        refrescarCantidad();

    }

    public void btnGuardar_action() throws IOException {

        // se extraen los datos ingresados en cada campo de texto y se eliminan los espacios a izquierda y derecha
        String pesoIngresado = txtPeso.getText().trim();
        String codigoIngresado = txtCodigo.getText().trim();
        String estadoSaludIngresado = txtEstadoSalud.getText().trim();
        String date ="";

        if(dpFecha.getValue() != null) {
            date = dpFecha.getValue().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        }
        // se valida que el codigo contenga un valor
        if (codigoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro", "El codigo es requerido");
            txtCodigo.requestFocus();
            return;
        }
        // se valida que el peso contenga un valor
        if (pesoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro", "El peso es requerido");
            txtPeso.requestFocus();
            return;
        }
        // se valida que el peso sea un número
        try {
            if (Integer.parseInt(pesoIngresado) <= 0){
                mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro",  "El peso debe ser un numero mayor que 0");
                txtPeso.requestFocus();
                txtPeso.clear();
                return;
            }
        } catch (NumberFormatException numberFormatException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro", "El peso debe ser un valor numérico");
            txtPeso.requestFocus();
            txtPeso.clear();
            return;
        }
        // se valida que el estado de salud contenga un valor
        if (estadoSaludIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro", "El estado de salud es requerido");
            txtEstadoSalud.requestFocus();
            return;
        }
        // se valida que el precio por kg contenga un valor
        if (date.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro de marrano", "Resultado del registro", "La fecha es requerida");
            dpFecha.requestFocus();
            return;
        }

        float pesoIngresadoFloat = Float.parseFloat(pesoIngresado);

        Marrano marrano = new Marrano(codigoIngresado, pesoIngresadoFloat, estadoSaludIngresado, dpFecha.getValue(),this.grupoAnimalBsn.codigoGeneral(),this.grupoAnimalBsn.dietaGeneral());

        this.marranoBsn.registrarMarrano(marrano);
        mostrarMensaje(Alert.AlertType.INFORMATION, "Registro de marrano", "Resultado del registro", "El marrano ha sido registrado con éxito");
        limpiarCampos();
        refrescarCantidad();

    }

    private void limpiarCampos() {
        this.txtPeso.clear();
        this.txtEstadoSalud.clear();
        this.txtCodigo.clear();
        this.dpFecha.getEditor().clear();
        this.dpFecha.setValue(null);
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {

        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    public void btnEnfermo_action(){
        txtEstadoSalud.clear();
        txtEstadoSalud.setEditable(false);
        txtEstadoSalud.setVisible(true);
        txtEstadoSalud.setText("Enfermo");
    }

    public void btnSano_action(){
        txtEstadoSalud.clear();
        txtEstadoSalud.setEditable(false);
        txtEstadoSalud.setVisible(true);
        txtEstadoSalud.setText("Sano");
    }


    private void refrescarCantidad() throws IOException{
        try {
            marranoBsn.verificarCantidad();
            mostrarMensaje(Alert.AlertType.INFORMATION, "WARNING", "Informacion", "Primero tiene que registar un grupo general de marranos");
            Parent ventanaRegistrarMarrano = FXMLLoader.load(getClass().getResource("../view/registrar-grupo-animal.fxml"));
            this.vistaRegistro.setCenter(ventanaRegistrarMarrano);
        }
        catch (FaltaRegistrarMarranosException e) {
        }
    }
}

