package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.TomateDatosGeneralesBSN;
import co.edu.udea.tecnicas.fincapp.bsn.TomatesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class RegistrarTomateController {

    @FXML
    private BorderPane contenedorRegistroTmt;
    @FXML
    private TextField txtCodigoLoteTmt;
    @FXML
    private TextField txtTipoFertTmt;
    @FXML
    private TextField txtTipoAbonoTmt;
    @FXML
    private TextField txtTipoInsecTmt;
    @FXML
    private TextField txtCantidadTmt;
    @FXML
    private TextField txtFecha;
    @FXML
    private SplitMenuButton smbFertilizanteTmt;
    @FXML
    private MenuItem mitOtroFertilizante;
    @FXML
    private SplitMenuButton smbAbonoTmt;
    @FXML
    private MenuItem mitOtroAbono;
    @FXML
    private SplitMenuButton smbInsecticidaTmt;
    @FXML
    private MenuItem mitOtroInsecticida;

    private TomatesBsn tomatesBsn;
    private TomateDatosGeneralesBSN tomateDatosGeneralesBSN;
    private List<DatosGeneralesCultivos> datosGeneralesCultivosList;
    private List<String> tiposDeFertilizante;
    private List<String> tiposDeAbono;
    private List<String> tiposDeInsecticidas;
    private String codigoGeneral;
    private int ultimoCodigo;
    private final String estado = "INGRESADO";

    @FXML
    public void initialize() throws IOException {
        refrescarCodigoFecha();
        refrescarSpliMenu();
        txtCantidadTmt.setTextFormatter(new TextFormatter<>(change ->{
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length()<=3){
                return change;
            }
            return null;
        }));
    }

    public RegistrarTomateController() {
        this.tomatesBsn = new TomatesBsn();
        this.tomateDatosGeneralesBSN = new TomateDatosGeneralesBSN();
    }

    public void btnGuardarTmt_action() throws IOException{

        String tipoFertIngresadoTmt = txtTipoFertTmt.getText().trim();
        String tipoAbonoIngresadoTmt = txtTipoAbonoTmt.getText().trim();
        String tipoInsecIngresadoTmt = txtTipoInsecTmt.getText().trim();
        String cantidadTmtIngresado = txtCantidadTmt.getText().trim();


        //Validacion de que los datos tengan un valor

        if (tipoFertIngresadoTmt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro tomate", "Error en los datos", "El tipo de fertilizante es requerido para registrar un tomate");
            txtTipoFertTmt.requestFocus();
            return;
        }

        if (tipoAbonoIngresadoTmt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro tomate", "Error en los datos", "El tipo de abono es requerido para registrar un tomate");
            txtTipoAbonoTmt.requestFocus();
            return;
        }

        if (tipoInsecIngresadoTmt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro tomate", "Error en los datos", "El tipo de insecticida es requerido para registrar un tomate");
            txtTipoInsecTmt.requestFocus();
            return;
        }

        try {
            Integer.parseInt(cantidadTmtIngresado);
        } catch (NumberFormatException exception) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro tomate", "Error en los datos", "La cantidad de tomates registrados es necesaria para completar el registro");
            txtCantidadTmt.requestFocus();
            txtCantidadTmt.clear();
            return;
        }


        for (int i = 1; i <= Integer.parseInt(cantidadTmtIngresado); i++) {
            StringBuilder codigoLoteTmt = new StringBuilder();
            ultimoCodigo++;
            codigoLoteTmt.append(codigoGeneral)
                    .append(ultimoCodigo);
            this.tomatesBsn.registrarTomate(codigoLoteTmt.toString(), tipoFertIngresadoTmt, tipoAbonoIngresadoTmt, tipoInsecIngresadoTmt, LocalDate.now(), estado);
        }
        limpiarCampos();
        refrescarCodigoFecha();
        refrescarSpliMenu();
    }

    public void btnAgregarOpcionFertilizante_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoFertTmt.setText(menuInvocador.getText());
        txtTipoFertTmt.setVisible(true);
        txtTipoFertTmt.setEditable(false);
    }

    public void btnAgregarOpcionAbono_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoAbonoTmt.setText(menuInvocador.getText());
        txtTipoAbonoTmt.setVisible(true);
        txtTipoAbonoTmt.setEditable(false);
    }

    public void btnAgregarOpcionInsecticida_action(ActionEvent actionEvent) {
        MenuItem menuInvocador = (MenuItem) actionEvent.getSource();
        txtTipoInsecTmt.setText(menuInvocador.getText());
        txtTipoInsecTmt.setVisible(true);
        txtTipoInsecTmt.setEditable(false);
    }

    public void btnOtroFertilizante_action() {
        txtTipoFertTmt.clear();
        txtTipoFertTmt.setEditable(true);
        txtTipoFertTmt.setVisible(true);
        txtTipoFertTmt.requestFocus();
    }

    public void btnOtroAbono_action() {
        txtTipoAbonoTmt.clear();
        txtTipoAbonoTmt.setEditable(true);
        txtTipoAbonoTmt.setVisible(true);
        txtTipoAbonoTmt.requestFocus();
    }

    public void btnOtroInsecticida_action() {
        txtTipoInsecTmt.clear();
        txtTipoInsecTmt.setEditable(true);
        txtTipoInsecTmt.setVisible(true);
        txtTipoInsecTmt.requestFocus();
    }

    private void limpiarCampos() {
        this.txtTipoInsecTmt.clear();
        this.txtTipoFertTmt.clear();
        this.txtTipoAbonoTmt.clear();
        this.txtCantidadTmt.clear();
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }

    private void refrescarCodigoFecha() throws IOException {
        //Se visualiza en la vista la fecha del registro
        this.txtFecha.setText(LocalDate.now().toString());
        try {
            datosGeneralesCultivosList = this.tomateDatosGeneralesBSN.consultarDatosGeneralesTomates();
            codigoGeneral = datosGeneralesCultivosList.get(0).getCodigoGeneral();
            ultimoCodigo = this.tomatesBsn.codigoUltimoTomateRegistrado();
            int siguienteCodigo = ultimoCodigo + 1;
            txtCodigoLoteTmt.setText(codigoGeneral + siguienteCodigo); //Se cambia a automatico
        } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
            mostrarMensaje(Alert.AlertType.ERROR, "Registro tomates", "Error en datos generales", datosGeneralesRequeridosException.getMessage());
            Parent ventanaDatosGeneralesTmt = FXMLLoader.load(getClass().getResource("../view/datos-generales-tomates.fxml"));
            this.contenedorRegistroTmt.setCenter(ventanaDatosGeneralesTmt);
        }
    }

    private void refrescarSpliMenu() {
        //Elimina las opciones que estaban antes para evitar duplicados
        for (MenuItem opcion : smbFertilizanteTmt.getItems()) {
            smbFertilizanteTmt.getItems().retainAll(opcion);
        }
        for (MenuItem opcion : smbAbonoTmt.getItems()) {
            smbAbonoTmt.getItems().retainAll(opcion);
        }
        for (MenuItem opcion : smbInsecticidaTmt.getItems()) {
            smbInsecticidaTmt.getItems().retainAll(opcion);
        }

        //Se intentan buscar tipos de datos ya ingresados para añadirlos al splitmenu
        try {
            tiposDeFertilizante = this.tomatesBsn.tiposFertilizantesRegistradosTmt();
            for (String tipoDeFertilizante : tiposDeFertilizante) {
                MenuItem nuevaOpcion = new MenuItem(tipoDeFertilizante);
                smbFertilizanteTmt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionFertilizante_action);
            }
            mitOtroFertilizante.setVisible(true);
            txtTipoFertTmt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroFertilizante.setVisible(false);
        }
        try {
            tiposDeAbono = this.tomatesBsn.tiposAbonosRegistradosTmt();
            for (String tiposDeAbono : tiposDeAbono) {
                MenuItem nuevaOpcion = new MenuItem(tiposDeAbono);
                smbAbonoTmt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionAbono_action);
            }
            mitOtroAbono.setVisible(true);
            txtTipoAbonoTmt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroAbono.setVisible(false);
        }
        try {
            tiposDeInsecticidas = this.tomatesBsn.tiposInsecticidasRegistradosTmt();
            for (String tiposDeInsecticida : tiposDeInsecticidas) {
                MenuItem nuevaOpcion = new MenuItem(tiposDeInsecticida);
                smbInsecticidaTmt.getItems().add(nuevaOpcion);
                nuevaOpcion.setOnAction(this::btnAgregarOpcionInsecticida_action);
            }
            mitOtroInsecticida.setVisible(true);
            txtTipoInsecTmt.setVisible(false);
        } catch (NoHayDatosException noHayDatosException) {
            mitOtroInsecticida.setVisible(false);
        }
    }
}