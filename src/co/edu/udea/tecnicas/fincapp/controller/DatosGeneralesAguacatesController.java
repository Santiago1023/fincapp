package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.AguacateDatosGeneralesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosIncorrectosException;
import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

import java.util.List;
import java.util.Optional;

public class DatosGeneralesAguacatesController {

    @FXML
    private GridPane gpDatos;
    @FXML
    private TextField txtPrecioKgAgt;
    @FXML
    private TextField txtPrecioSemillaAgt;
    @FXML
    private TextField txtCodigoGeneralAgt;
    @FXML
    private TextField txtUbicacionAgt;
    @FXML
    private ButtonBar btnbarCambios;
    @FXML
    private Button btnGuardar;
    @FXML
    private TableView<DatosGeneralesCultivos> tblDatosAgt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlPrecioKgAgt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlPrecioSemillaAgt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlCodigoAgt;
    @FXML
    private TableColumn<DatosGeneralesCultivos, String> cmlUbicacionAgt;
    @FXML
    private Label lblCambios;

    private AguacateDatosGeneralesBsn aguacateDatosGeneralesBsn;

    public DatosGeneralesAguacatesController() {
        this.aguacateDatosGeneralesBsn = new AguacateDatosGeneralesBsn();
    }

    @FXML
    public void initialize() {
        refrescar();
    }

    public void btnGuardarPrecioAgt_action() {

        String precioKgIngresadoAgt = txtPrecioKgAgt.getText().trim();
        String precioSemillaIngresadoAgt = txtPrecioSemillaAgt.getText().trim();
        String ubicacionIngresadoAgt = txtUbicacionAgt.getText().trim();
        String codigoGeneralIngresadoAgt = txtCodigoGeneralAgt.getText().trim();

        try {
            if (Float.parseFloat(precioKgIngresadoAgt) < 0) {
                mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "El precio del kg es requerido y debe ser numerico positivo");
                txtPrecioKgAgt.requestFocus();
                txtPrecioKgAgt.clear();
                return;
            }
        } catch (NumberFormatException exception) {
            //exception.printStackTrace();
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "El precio del kg es requerido y debe ser numerico");
            txtPrecioKgAgt.requestFocus();
            txtPrecioKgAgt.clear();
            return;
        }

        try {
            if (Float.parseFloat(precioSemillaIngresadoAgt) < 0) {
                mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "El precio de la semilla es requerido y debe ser numerico positivo");
                txtPrecioSemillaAgt.requestFocus();
                txtPrecioSemillaAgt.clear();
                return;
            }

        } catch (NumberFormatException exception) {
            //exception.printStackTrace();
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "El precio de la semilla es requerido y debe ser numerico");
            txtPrecioSemillaAgt.requestFocus();
            txtPrecioSemillaAgt.clear();
            return;
        }

        if (codigoGeneralIngresadoAgt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "El codigo general es requerido");
            txtCodigoGeneralAgt.requestFocus();
            return;
        }

        if (ubicacionIngresadoAgt.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", "La ubicación es requerida");
            txtUbicacionAgt.requestFocus();
            return;
        }

        try {
            this.aguacateDatosGeneralesBsn.registrarDatosGeneralesAguacate(precioKgIngresadoAgt, precioSemillaIngresadoAgt, ubicacionIngresadoAgt, codigoGeneralIngresadoAgt);
        } catch (DatosIncorrectosException exception) {
            mostrarMensaje(Alert.AlertType.ERROR, "Datos generales aguacate", "Error en los datos", exception.getMessage());
        }

        refrescar();
        limpiarCampos();
    }

    public void btnCambiarPrecios_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio precios", "Esta seguro que desea cambiar los precios generales de los aguacates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = aguacateDatosGeneralesBsn.consultarDatosGeneralesAguacates();
                txtCodigoGeneralAgt.setText(datosGeneralesCultivosList.get(0).getCodigoGeneral());
                txtUbicacionAgt.setText(datosGeneralesCultivosList.get(0).getUbicacion());
                txtPrecioKgAgt.setEditable(true);
                txtPrecioSemillaAgt.setEditable(true);
                txtCodigoGeneralAgt.setEditable(false);
                txtUbicacionAgt.setEditable(false);
                btnGuardar.setVisible(true);
                btnbarCambios.setVisible(false);
                gpDatos.setVisible(true);
                lblCambios.setVisible(false);
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarCodigo_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio codigo general", "Esta seguro que desea cambiar el codigo general de los aguacates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = aguacateDatosGeneralesBsn.consultarDatosGeneralesAguacates();
                txtPrecioKgAgt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioKg()));
                txtPrecioSemillaAgt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioSemilla()));
                txtUbicacionAgt.setText(datosGeneralesCultivosList.get(0).getUbicacion());
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
            txtPrecioKgAgt.setEditable(false);
            txtPrecioSemillaAgt.setEditable(false);
            txtCodigoGeneralAgt.setEditable(true);
            txtUbicacionAgt.setEditable(false);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarUbicacion_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambio ubicacion", "Esta seguro que desea cambiar la ubicacion general de los aguacates")) {
            try {
                List<DatosGeneralesCultivos> datosGeneralesCultivosList = aguacateDatosGeneralesBsn.consultarDatosGeneralesAguacates();
                txtPrecioKgAgt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioKg()));
                txtPrecioSemillaAgt.setText(Float.toString(datosGeneralesCultivosList.get(0).getPrecioSemilla()));
                txtCodigoGeneralAgt.setText(datosGeneralesCultivosList.get(0).getCodigoGeneral());
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
            }
            //Podria meter esto en un metodo
            txtPrecioKgAgt.setEditable(false);
            txtPrecioSemillaAgt.setEditable(false);
            txtCodigoGeneralAgt.setEditable(false);
            txtUbicacionAgt.setEditable(true);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    public void btnCambiarTodo_action() {
        if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Cambio datos generales", "Cambiar todo", "Esta seguro que desea cambiar todo los datos generales de los aguacates")) {
            txtPrecioKgAgt.setEditable(true);
            txtPrecioSemillaAgt.setEditable(true);
            txtCodigoGeneralAgt.setEditable(true);
            txtUbicacionAgt.setEditable(true);
            btnGuardar.setVisible(true);
            btnbarCambios.setVisible(false);
            gpDatos.setVisible(true);
            lblCambios.setVisible(false);
        } else {
            btnGuardar.setVisible(false);
            btnbarCambios.setVisible(true);
            lblCambios.setVisible(true);
            gpDatos.setVisible(false);
        }
    }

    private boolean mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        if (tipo == Alert.AlertType.CONFIRMATION) {
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                return true;
            } else {
                return false;
            }
        } else {
            alert.showAndWait();
        }
        return false;
    }

    private void refrescar() {
        cmlPrecioKgAgt.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPrecioKg())));
        cmlPrecioSemillaAgt.setCellValueFactory(cellData -> new SimpleStringProperty(Float.toString(cellData.getValue().getPrecioSemilla())));
        cmlUbicacionAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getUbicacion()));
        cmlCodigoAgt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoGeneral()));
        //Toco comentarlo porque no deja cambiar algo especifico
        /*
        txtPrecioKgAgt.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 4) {
                return change;
            }
            return null;
        }));
        txtPrecioSemillaAgt.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 3) {
                return change;
            }
            return null;
        }));
         */
        try {
            List<DatosGeneralesCultivos> datosGeneralesCultivosList = aguacateDatosGeneralesBsn.consultarDatosGeneralesAguacates();
            ObservableList<DatosGeneralesCultivos> datosGeneralesCultivosObservableList = FXCollections.observableList(datosGeneralesCultivosList);
            tblDatosAgt.setItems(datosGeneralesCultivosObservableList);
            tblDatosAgt.getItems().removeAll();

            if (datosGeneralesCultivosList.size() != 0) {
                lblCambios.setVisible(true);
                tblDatosAgt.setVisible(true);
                btnbarCambios.setVisible(true);
                btnGuardar.setVisible(false);
                gpDatos.setVisible(false);
            } else {
                gpDatos.setVisible(true);
                btnGuardar.setVisible(true);
            }
        } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Registro datos generales", "Datos generales", datosGeneralesRequeridosException.getMessage());
        }
    }

    private void limpiarCampos() {
        this.txtPrecioSemillaAgt.clear();
        this.txtPrecioKgAgt.clear();
        this.txtUbicacionAgt.clear();
        this.txtCodigoGeneralAgt.clear();
    }
}
