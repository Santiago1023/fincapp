package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.TomatesBsn;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.model.Tomate;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.time.LocalDate;
import java.util.List;

public class MostrarTomatesController {
    @FXML
    private Label lblTitulo;
    @FXML
    private TableView<Tomate> tblTomates;
    @FXML
    private TableColumn<Tomate, String> cmlTipoTmt;
    @FXML
    private TableColumn<Tomate, String> cmlFertilizanteTmt;
    @FXML
    private TableColumn<Tomate, String> cmlAbonoTmt;
    @FXML
    private TableColumn<Tomate, String> cmlInsecticidaTmt;
    @FXML
    private TableColumn<Tomate, LocalDate> cmlFecha;
    @FXML
    private TableColumn<Tomate, String> cmlEstado;

    private TomatesBsn tomatesBsn;

    public MostrarTomatesController() {
        this.tomatesBsn = new TomatesBsn();
    }

    @FXML
    public void initialize() {
        cmlTipoTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCodigoLote()));
        cmlFertilizanteTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getFertilizante()));
        cmlAbonoTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getAbono()));
        cmlInsecticidaTmt.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getInsecticida()));
        cmlFecha.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getFecha()));
        cmlEstado.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().getEstado()));
        btnMostrarTodos_action();
    }

    public void btnMostrarVendidos_action() {
        try {
            List<Tomate> tomatesList = tomatesBsn.consultarTomatesVendidos();
            ObservableList<Tomate> tomatesListObs = FXCollections.observableList(tomatesList);
            tblTomates.setItems(tomatesListObs);
            lblTitulo.setText("TOMATES VENDIDOS");
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Tomates registrados", "No hay tomates vendidos", noHayDatosException.getMessage());
        }
    }

    public void btnMostrarIngresados_action() {
        try {
            List<Tomate> tomatesList = tomatesBsn.consultarTomatesIngresados();
            ObservableList<Tomate> tomatesListObs = FXCollections.observableList(tomatesList);
            tblTomates.setItems(tomatesListObs);
            lblTitulo.setText("TOMATES INGRESADOS");
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Tomates registrados", "No hay tomates registrados", noHayDatosException.getMessage());
        }
    }

    public void btnMostrarTodos_action() {
        lblTitulo.setText("TOMATES");
        try {
            List<Tomate> tomatesList = tomatesBsn.consultarTomates();
            ObservableList<Tomate> tomatesListObs = FXCollections.observableList(tomatesList);
            tblTomates.setItems(tomatesListObs);
        } catch (NoHayDatosException noHayDatosException) {
            mostrarMensaje(Alert.AlertType.INFORMATION, "Tomates registrados", "No hay tomates ingresados", noHayDatosException.getMessage());
        }
    }

    private void mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        alert.showAndWait();
    }
}
