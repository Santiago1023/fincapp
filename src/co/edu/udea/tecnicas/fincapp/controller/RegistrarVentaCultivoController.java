package co.edu.udea.tecnicas.fincapp.controller;

import co.edu.udea.tecnicas.fincapp.bsn.*;
import co.edu.udea.tecnicas.fincapp.bsn.exception.DatosGeneralesRequeridosException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayCantidadException;
import co.edu.udea.tecnicas.fincapp.bsn.exception.NoHayDatosException;
import co.edu.udea.tecnicas.fincapp.dao.VentaDAO;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;

import java.time.LocalDate;
import java.util.Optional;

public class RegistrarVentaCultivoController {
    //Se podria hacer que quede con un dato completo de aguacate, proximamente si el tiempo da

    @FXML
    private AnchorPane apVenta;
    @FXML
    private Label lblCantidad;
    @FXML
    private TextField txtFecha;
    @FXML
    private TextField txtKgVenta;
    @FXML
    private TextField txtCantidad;
    @FXML
    private TextField txtCodigoFactura;
    @FXML
    private TextField txtProducto;
    @FXML
    private MenuItem miAgt;
    @FXML
    private MenuItem miTmt;

    private VentaBsn ventaBsn;
    private AguacatesBsn aguacatesBsn;
    private TomatesBsn tomatesBsn;
    private AguacateDatosGeneralesBsn aguacateDatosGeneralesBsn;
    private TomateDatosGeneralesBSN tomateDatosGeneralesBSN;
    private final String CodigoFactura = "FACT";
    private int ultimaFactura;

    @FXML
    public void initialize() {
        refrescar();
    }

    public RegistrarVentaCultivoController() {
        this.ventaBsn = new VentaBsn();
        this.aguacatesBsn = new AguacatesBsn();
        this.aguacateDatosGeneralesBsn = new AguacateDatosGeneralesBsn();
        this.tomatesBsn = new TomatesBsn();
        this.tomateDatosGeneralesBSN = new TomateDatosGeneralesBSN();
    }

    public void btnAguacate_action() {
        this.txtProducto.setText("AGUACATE");
        this.lblCantidad.setText("CANTIDAD AGUACATES");
    }

    public void btnTomate_action() {
        this.txtProducto.setText("TOMATE");
        this.lblCantidad.setText("CANTIDAD TOMATES");
    }

    public void btnConfirmarCompra_action() {

        String codigo = txtCodigoFactura.getText();
        String productoIngresado = txtProducto.getText();
        int cantidadIngresada = Integer.parseInt(txtCantidad.getText().trim());
        float kgVentaIngresado = Float.parseFloat(txtKgVenta.getText().trim());

        if (productoIngresado.isEmpty()) {
            mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Dato necesario", "Seleccione el producto para realizar la venta");
            txtProducto.requestFocus();
            return;
        }
        //Ya no se necesitaria validar que si sea numerico porque solo permite numeros
        //Vende los primeros aguacates que encuentra

        if (productoIngresado.equals("AGUACATE")){
            try {
                float precioVenta = this.aguacateDatosGeneralesBsn.consultarPrecioKg() * kgVentaIngresado;
                if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Venta", "Precio total de la venta", Float.toString(precioVenta))) {
                    try {
                        this.aguacatesBsn.venderAguacates(cantidadIngresada);
                    } catch (NoHayCantidadException e) {
                        try {
                            int cantidadDisponibles =this.aguacatesBsn.consultarAguacatesIngresados().size();
                            mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Producto insuficiente", e.getMessage() + ". La cantidad disponible del producto es de: " + cantidadDisponibles );
                            txtCantidad.clear();
                            txtCantidad.requestFocus();
                            return;
                        } catch (NoHayDatosException noHayDatosException) {
                            mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Producto insuficiente", e.getMessage() + ". No hay existencias del producto");
                            limpiarCampos();
                            return;
                        }
                    }
                    this.ventaBsn.registrarVenta(codigo, productoIngresado, cantidadIngresada, precioVenta, LocalDate.now());
                }
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Datos generales requeridos", datosGeneralesRequeridosException.getMessage());
                apVenta.setVisible(false);
                return;
            }
        }
        if (productoIngresado.equals("TOMATE")){
            try {
                float precioVenta = this.tomateDatosGeneralesBSN.consultarPrecioKg() * kgVentaIngresado;
                if (mostrarMensaje(Alert.AlertType.CONFIRMATION, "Venta", "Precio total de la venta", Float.toString(precioVenta))) {
                    try {
                        this.tomatesBsn.venderTomates(cantidadIngresada);
                    } catch (NoHayCantidadException e) {
                        try {
                            int cantidadDisponibles =this.tomatesBsn.consultarTomatesIngresados().size();
                            mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Producto insuficiente", e.getMessage() + ". La cantidad disponible del producto es de: " + cantidadDisponibles );
                            txtCantidad.clear();
                            txtCantidad.requestFocus();
                            return;
                        } catch (NoHayDatosException noHayDatosException) {
                            mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Producto insuficiente", e.getMessage() + ". No hay existencias del producto");
                            limpiarCampos();
                            return;
                        }
                    }
                    this.ventaBsn.registrarVenta(codigo, productoIngresado, cantidadIngresada, precioVenta, LocalDate.now());
                }
            } catch (DatosGeneralesRequeridosException datosGeneralesRequeridosException) {
                mostrarMensaje(Alert.AlertType.ERROR, "Venta", "Datos generales requeridos", datosGeneralesRequeridosException.getMessage());
                apVenta.setVisible(false);
                return;
            }
        }

        limpiarCampos();
        refrescar();
    }

    private void limpiarCampos() {
        txtCantidad.clear();
        txtKgVenta.clear();
        txtProducto.clear();
    }

    private boolean mostrarMensaje(Alert.AlertType tipo, String titulo, String encabezado, String mensaje) {
        Alert alert = new Alert(tipo);
        alert.setTitle(titulo);
        alert.setHeaderText(encabezado);
        alert.setContentText(mensaje);
        if (tipo == Alert.AlertType.CONFIRMATION) {
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK) {
                return true;
            } else {
                return false;
            }
        } else {
            alert.showAndWait();
        }
        return false;
    }

    private void refrescar() {
        apVenta.setVisible(true);
        lblCantidad.setText("CANTIDAD");
        txtFecha.setText(LocalDate.now().toString());
        ultimaFactura = this.ventaBsn.codigoUltimaVenta();
        int siguienteFactura = ultimaFactura + 1;
        txtCodigoFactura.setText(CodigoFactura + siguienteFactura);
        txtCantidad.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 2) {
                return change;
            }
            return null;
        }));
        txtKgVenta.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 4) {
                return change;
            }
            return null;
        }));
    }
}
