package co.edu.udea.tecnicas.fincapp.model;

public class DatosGeneralesCultivos {

    private float precioKg;
    private float precioSemilla;
    private String ubicacion;
    private String codigoGeneral;

    public DatosGeneralesCultivos(float precioKg, float precioSemilla, String ubicacion, String codigoGeneral){
        this.precioKg = precioKg;
        this.precioSemilla = precioSemilla;
        this.ubicacion = ubicacion;
        this.codigoGeneral = codigoGeneral;
    }

    public float getPrecioKg() {
        return precioKg;
    }

    public void setPrecioKg(float precioKg) {
        this.precioKg = precioKg;
    }

    public float getPrecioSemilla() {
        return precioSemilla;
    }

    public void setPrecioSemilla(float precioSemilla) {
        this.precioSemilla = precioSemilla;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getCodigoGeneral() {
        return codigoGeneral;
    }

    public void setCodigoGeneral(String codigoGeneral) {
        this.codigoGeneral = codigoGeneral;
    }
}
