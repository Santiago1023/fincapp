package co.edu.udea.tecnicas.fincapp.model;

import java.time.LocalDate;

public class Venta {
    //dd/mm/yyyy dd-mm-yyyy
    private String codigoFactura;
    private String producto;
    private int cantidad;
    private float precioVenta; // precioVenta = pesoKgDelMarrano * precioKgDatosGenerales
    private LocalDate fecha;

    //precio de venta debe de traer de los datos generales el precio del Kg y
    //En el view no se toca el precio de venta sino que nosotros lo generamos
    //podemos hacer una confirmacion
    public Venta(String codigoFactura, String producto, int cantidad, float precioVenta, LocalDate fecha) {
        this.codigoFactura = codigoFactura;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precioVenta = precioVenta;
        this.fecha = fecha;
    }

    public String getCodigoFactura() {
        return codigoFactura;
    }

    public void setCodigoFactura(String codigoFactura) {
        this.codigoFactura = codigoFactura;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public float getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(float precioVenta) {
        this.precioVenta = precioVenta;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
}
