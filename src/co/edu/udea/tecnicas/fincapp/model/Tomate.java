package co.edu.udea.tecnicas.fincapp.model;

import java.time.LocalDate;

public class Tomate{

    // es POJO?
    private String codigoLote;
    private String fertilizante;
    private String abono;
    private String insecticida;
    private LocalDate fecha;
    private String estado;


    public Tomate(String codigoLote, String fertilizante, String abono, String insecticida, LocalDate fecha, String estado) {
        this.codigoLote = codigoLote;
        this.fertilizante = fertilizante;
        this.abono = abono;
        this.insecticida = insecticida;
        this.fecha = fecha;
        this.estado = estado;
    }

    public String getCodigoLote() {
        return codigoLote;
    }

    public void setCodigoLote(String codigoLote) {
        this.codigoLote = codigoLote;
    }

    public String getFertilizante() {
        return fertilizante;
    }

    public void setFertilizante(String fertilizante) {
        this.fertilizante = fertilizante;
    }

    public String getAbono() {
        return abono;
    }

    public void setAbono(String abono) {
        this.abono = abono;
    }

    public String getInsecticida() {
        return insecticida;
    }

    public void setInsecticida(String insecticida) {
        this.insecticida = insecticida;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
