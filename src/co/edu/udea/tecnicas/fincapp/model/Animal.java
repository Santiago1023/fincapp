package co.edu.udea.tecnicas.fincapp.model;

import java.util.List;

public abstract class Animal {

    // es POJO?

    private String estadoSalud;
    private String edad;
    private String lugar;
    private String peso;
    private String alimento;
    private String vacuna;
    private String precioKg;

    public Animal(String peso,  String alimento,String vacuna, String precioKg, String estadoSalud, String edad, String lugar) {
        this.peso = peso;
        this.alimento = alimento;
        this.vacuna = vacuna;
        this.precioKg = precioKg;
        this.estadoSalud = estadoSalud;
        this.edad = edad;
        this.lugar = lugar;
    }

    public Animal(){

    }

    public String getEstadoSalud() {
        return estadoSalud;
    }

    public void setEstadoSalud(String estadoSalud) {
        this.estadoSalud = estadoSalud;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getAlimento() {
        return alimento;
    }

    public void setAlimento(String alimento) {
        this.alimento = alimento;
    }

    public String getVacuna() {
        return vacuna;
    }

    public void setVacuna(String vacuna) {
        this.vacuna = vacuna;
    }

    public String getPrecioKg() {
        return precioKg;
    }

    public void setPrecioKg(String precioKg) {
        this.precioKg = precioKg;
    }
}
