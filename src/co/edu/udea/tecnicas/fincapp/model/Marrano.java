package co.edu.udea.tecnicas.fincapp.model;

import java.time.LocalDate;
import java.util.List;

public class Marrano implements Cloneable{

    String codigo;
    float peso;
    String estadoSalud;
    LocalDate fechaNacimiento;
    String codigoGeneral;
    String dietaGeneral;
    // es POJO?


    public Marrano(String codigo, float peso, String estadoSalud, LocalDate fechaNacimiento, String codigoGeneral, String dietaGeneral ) {
        this.codigo = codigo;
        this.peso = peso;
        this.estadoSalud = estadoSalud;
        this.fechaNacimiento = fechaNacimiento;
        this.codigoGeneral = codigoGeneral;
        this.dietaGeneral = dietaGeneral;

    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getEstadoSalud() {
        return estadoSalud;
    }

    public void setEstadoSalud(String estadoSalud) {
        this.estadoSalud = estadoSalud;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCodigoGeneral() {
        return codigoGeneral;
    }

    public void setCodigoGeneral(String codigoGeneral) {
        this.codigoGeneral = codigoGeneral;
    }

    public String getDietaGeneral() {
        return dietaGeneral;
    }

    public void setDietaGeneral(String dietaGeneral) {
        this.dietaGeneral = dietaGeneral;
    }

    public Marrano(){}
}
