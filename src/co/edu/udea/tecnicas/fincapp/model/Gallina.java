package co.edu.udea.tecnicas.fincapp.model;

import java.util.List;

public class Gallina extends Animal implements Cloneable{

    // es POJO?


    public Gallina(String peso, String alimento, String vacuna, String precioKg, String estadoSalud, String edad, String lugar) {
        super(peso, alimento, vacuna, precioKg, estadoSalud, edad, lugar);
    }

    public Gallina() {

    }
}
