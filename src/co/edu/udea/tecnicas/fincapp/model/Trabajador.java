package co.edu.udea.tecnicas.fincapp.model;

public class Trabajador extends Persona {

    // es POJO?
    private String horario; // no creo que sea taaann importante
    private String especialidad;
    private String eps;
    private String contacto;
    private String contactoEmergencia;
    private double salario;

    public Trabajador(String nombre, String apellido, String sexo, String documento) {
        super(nombre, apellido, sexo, documento);
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getEps() {
        return eps;
    }

    public void setEps(String eps) {
        this.eps = eps;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getContactoEmergencia() {
        return contactoEmergencia;
    }

    public void setContactoEmergencia(String contactoEmergencia) {
        this.contactoEmergencia = contactoEmergencia;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }
}
