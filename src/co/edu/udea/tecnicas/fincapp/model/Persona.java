package co.edu.udea.tecnicas.fincapp.model;

public abstract class Persona {

    // es POJO?

    private String nombre;
    private String apellido;
    private String sexo;
    private String documento;

    public Persona(String nombre, String apellido, String sexo, String documento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.documento = documento;
    }
}
