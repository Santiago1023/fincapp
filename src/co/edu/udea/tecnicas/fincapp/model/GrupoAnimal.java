package co.edu.udea.tecnicas.fincapp.model;

import java.time.LocalDate;

public class GrupoAnimal implements  Cloneable {

    LocalDate fechaRegistro;
    String tipo;
    int cantidad;
    String codigo;
    String ubicacion;
    String dieta;
    Double precioKg;

    public GrupoAnimal(String tipo, int cantidad, String codigo, String ubicacion, String dieta, Double precioKg, LocalDate fechaRegistro) {
        this.tipo = tipo;
        this.cantidad = cantidad;
        this.codigo = codigo;
        this.ubicacion = ubicacion;
        this.dieta = dieta;
        this.precioKg = precioKg;
        this.fechaRegistro = fechaRegistro;
    }
    public GrupoAnimal(){
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public String getDieta() {
        return dieta;
    }

    public void setDieta(String dieta) {
        this.dieta = dieta;
    }

    public Double getPrecioKg() {
        return precioKg;
    }

    public void setPrecioKg(Double precioKg) {
        this.precioKg = precioKg;
    }

    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public GrupoAnimal clone() throws CloneNotSupportedException{
        return (GrupoAnimal) super.clone();
    }

}

