package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.GrupoAnimal;

import java.util.List;
import java.util.Optional;

public interface GrupoAnimalDAO {

    public void registrarGrupoAnimal(GrupoAnimal grupoAnimal);

    public List<GrupoAnimal> mostrarGrupoAnimal();

    Optional<GrupoAnimal> consultarPorCodigo(String codigo);


    String codigoGeneral();
    String dietaGeneral();
    int cantidadGeneral();
}
