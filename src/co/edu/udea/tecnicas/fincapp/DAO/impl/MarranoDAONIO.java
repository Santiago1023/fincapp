package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.dao.MarranoDAO;
import co.edu.udea.tecnicas.fincapp.model.Marrano;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class MarranoDAONIO implements MarranoDAO {

    private final static String NOMBRE_ARCHIVO = "marranos";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static Integer LONGITUD_REGISTRO = 100;
    private final static Integer LONGITUD_CODIGO = 15;
    private final static Integer LONGITUD_PESO = 5;
    private final static Integer LONGITUD_ESTADOSALUD = 20;
    private final static Integer LONGITUD_FECHA = 10;
    private final static Integer LONGITUD_DIETA = 25;
    private final static Integer LONGITUD_CODIGOGENERAL = 25;

    public MarranoDAONIO() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarMarrano(Marrano marrano) {
        // Se obtiene la representación en String del marrano
        String registro = parseMarranoString(marrano);
        // Se extraen los bytes del registro
        byte[] datosRegistro = registro.getBytes();
        // Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        // se abre un canal hacia el archivo en modo APPEND (adjuntar datos al final del archivo)
        try(FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            // se escriben los datos del buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    /**
     * Recibe un objeto de tipo marrano y, con base en los tamaños de los campos, retorna un string
     *
     * @param marrano
     * @return
     */
    private String parseMarranoString(Marrano marrano) {
        StringBuilder sb = new StringBuilder();
        String pesoString = Float.toString(marrano.getPeso());
        String fechaString = marrano.getFechaNacimiento().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        sb.append(completarCampo(marrano.getCodigo(), LONGITUD_CODIGO))
                //.append(completarCampo(marrano.getDieta(), LONGITUD_DIETA))
                .append(completarCampo(pesoString, LONGITUD_PESO))
                .append(completarCampo(marrano.getEstadoSalud(), LONGITUD_ESTADOSALUD))
                .append(completarCampo(fechaString, LONGITUD_FECHA))
                .append(completarCampo(marrano.getDietaGeneral() , LONGITUD_DIETA))
                .append(completarCampo(marrano.getCodigoGeneral(),LONGITUD_CODIGOGENERAL));
        return sb.toString();
    }

    private String completarCampo(String valor, Integer longitudCampo) {
        // si el valor del campo supera su longitud, se recorta hasta la longitud máxima permitida
        if (valor.length() > longitudCampo) {
            return valor.substring(0, longitudCampo);
        }
        // regex: Regular expresion
        return String.format("%1$" + longitudCampo + "s", valor);
    }

    @Override
    public List<Marrano> consultarMarranos() {
        List<Marrano> marranos = new ArrayList<>();
        // yo creo que hay que hacer la conversion acá
        // se abre un canal hacia el archivo para leer bytes
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            // se encarga de capturar 70 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            // lee paquetes de bytes hasta que llegue al final del archivo
            while (sbc.read(buffer) > 0) {
                // ubica el apuntador del buffer en la posición inicial
                buffer.rewind();
                // decodifica los bytes usando el juego de caracteres por defecto del sistema operativo
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Marrano marrano = parseBufferMarrano(registro);
                marranos.add(marrano);
                //prepara al buffer para leer bytes del disco de nuevo
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return marranos;
    }

    @Override
    public int verificarCantidad(String codigo) {
        int cantidadMismoCodigo = 0;
        List<Marrano> marranos = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Marrano marrano = parseBufferMarrano(registro);
                if(marrano.getCodigoGeneral().equals(codigo)){
                    cantidadMismoCodigo++;
                }
                buffer.flip();
            }
    } catch (IOException e) {
            e.printStackTrace();
        }
        return cantidadMismoCodigo;
    }

    private Marrano parseBufferMarrano(CharBuffer registro) {
        Marrano marrano = new Marrano();

        String codigo = registro.subSequence(0, LONGITUD_CODIGO).toString().trim();
        marrano.setCodigo(codigo);
        registro.position(LONGITUD_CODIGO);
        registro = registro.slice();

        String peso = registro.subSequence(0, LONGITUD_PESO).toString().trim();
        float pesoFloat = Float.parseFloat(peso);
        marrano.setPeso(pesoFloat);
        registro.position(LONGITUD_PESO);
        registro = registro.slice();

        String estadoSalud = registro.subSequence(0, LONGITUD_ESTADOSALUD).toString().trim();
        marrano.setEstadoSalud(estadoSalud);
        registro.position(LONGITUD_ESTADOSALUD);
        registro = registro.slice();


        String fechaNacimiento = registro.subSequence(0, LONGITUD_FECHA).toString().trim();
        LocalDate fechaString = LocalDate.parse(fechaNacimiento, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        marrano.setFechaNacimiento(fechaString);
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();

        String dieta =  registro.subSequence(0, LONGITUD_DIETA).toString().trim();
        marrano.setDietaGeneral(dieta);
        registro.position(LONGITUD_DIETA);
        registro = registro.slice();

        String codigoGeneral = registro.subSequence(0, LONGITUD_CODIGOGENERAL).toString().trim();
        marrano.setCodigoGeneral(codigoGeneral);

        return marrano;
    }

}
