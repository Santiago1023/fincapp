package co.edu.udea.tecnicas.fincapp.dao.impl;

import co.edu.udea.tecnicas.fincapp.dao.VentaDAO;
import co.edu.udea.tecnicas.fincapp.model.Aguacate;
import co.edu.udea.tecnicas.fincapp.model.Venta;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;
import static java.time.temporal.ChronoUnit.DAYS;

public class VentaDAONIO implements VentaDAO {

    private final static String NOMBRE_ARCHIVO = "ventas";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final static Integer LONGITUD_REGISTRO = 53;
    private final static Integer LONGITUD_CODIGO_FACTURA = 20;
    private final static Integer LONGITUD_PRODUCTO = 10; //Marrano agucate tomate gallina 7 es lo maximo
    private final static Integer LONGITUD_CANTIDAD = 3;
    private final static Integer LONGITUD_PRECIO_VENTA = 10; //Hasta un millon en float
    private final static Integer LONGITUD_FECHA = 10;

    public VentaDAONIO(){
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarVenta(Venta venta) {
        //Se obtiene la representacion en string de venta
        String registro = parseVentaString(venta);
        //Se extraen los bytes del registro
        byte[] datoRegistro = registro.getBytes();
        //Se ponen los bytes en un buffer
        ByteBuffer byteBuffer = ByteBuffer.wrap(datoRegistro);
        //Se abre un canal hacia el archivo
        //WRITE para cambiar el archivo, APPEND para agregarle
        try (FileChannel fc = (FileChannel.open(ARCHIVO, APPEND))) {
            //Se escriben los datos en el buffer
            fc.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public List<Venta> consultarVentas() {
        List<Venta> ventas = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            //Captura 170 caracteres por vez
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                //ubica el apuntador en la posicon inicial
                buffer.rewind();
                //decodifica los bytes
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Venta venta = parseBufferVenta(registro);
                ventas.add(venta);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return ventas;
    }

    //aguacate.getEstado().equals(VENDIDO) && (fecha.compareTo(aguacate.getFecha())>10) compara es meses de diferencia no dias para diferencia de fechas mayores a 10 funciona con la fecha mas grande primero al revez da negativo
    //aguacate.getEstado().equals(VENDIDO) && (aguacate.getFecha().getMonthValue() == 1) compara los meses se podria usar para mirar las ventas en un mes especifico tambien sirve con dias
    //aguacate.getEstado().equals(VENDIDO) && (aguacate.getFecha().isBefore(fecha)) fechas antes de fecha sin incluir la fecha
    //aguacate.getEstado().equals(VENDIDO) && (aguacate.getFecha().isAfter(fecha)) fechas despues de fecha tampoco sin incluir fecha. Se puede combinar con el anterior para periodos
    //Para ventas en numeros de dias el primero, para un solo mes en especifico eñ segundo, para una franja de tiempo las dos ultimas
    //Se podria crear un modelo de ventas
    @Override
    public List<Venta> consultatVentasEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal) {
        List<Venta> ventas = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Venta venta = parseBufferVenta(registro);
                if ((venta.getFecha().isAfter(fechaInicio) && venta.getFecha().isBefore(fechaFinal)) || (venta.getFecha().isEqual(fechaFinal)) || (venta.getFecha().isEqual(fechaInicio))){
                    ventas.add(venta);
                }
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ventas;
    }

    @Override
    public List<Venta> consultatVentasDias(int numeroDias) {
        List<Venta> ventas = new ArrayList<>();
        LocalDate fechaActual = LocalDate.now();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Venta venta = parseBufferVenta(registro);
                System.out.println();
                if (DAYS.between(fechaActual,venta.getFecha()) <= numeroDias){
                    ventas.add(venta);
                }
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ventas;
    }

    @Override
    public List<Venta> consultarVentasMes(int mes) {
        List<Venta> ventas = new ArrayList<>();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Venta venta = parseBufferVenta(registro);
                if (venta.getFecha().getMonthValue() == mes){
                    ventas.add(venta);
                }
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ventas;
    }

    @Override
    public float valorVentasTotales() {
        List<Venta> ventas = consultarVentas();
        float valorVentasToles = 0;
        for (Venta venta : ventas){
            valorVentasToles += venta.getPrecioVenta();
        }
        return valorVentasToles;
    }

    @Override
    public float valorVentasEntreFechas(LocalDate fechaInicio, LocalDate fechaFinal) {
        List<Venta> ventas = consultatVentasEntreFechas(fechaInicio,fechaFinal);
        float valorVentasToles = 0;
        for (Venta venta : ventas){
            valorVentasToles += venta.getPrecioVenta();
        }
        return valorVentasToles;
    }

    @Override
    public float valorVentasDias(int numeroDias) {
        List<Venta> ventas = consultatVentasDias(numeroDias);
        float valorVentasToles = 0;
        for (Venta venta : ventas){
            valorVentasToles += venta.getPrecioVenta();
        }
        return valorVentasToles;
    }

    @Override
    public float valorVentasMes(int mes) {
        List<Venta> ventas = consultarVentasMes(mes);
        float valorVentasToles = 0;
        for (Venta venta : ventas){
            valorVentasToles += venta.getPrecioVenta();
        }
        return valorVentasToles;
    }

    @Override
    public int codigoUltimaFactura(){
        int ultimoCodigo = 0;
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                ultimoCodigo = ultimoValorCodigo(registro);
                buffer.flip();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ultimoCodigo;
    }

    private int ultimoValorCodigo(CharBuffer registro) {
        String codigo = registro.subSequence(0, LONGITUD_CODIGO_FACTURA).toString().trim();
        StringBuilder ultimoCodigo = new StringBuilder();
        for (int i = 0; i < codigo.length(); i++) {
            if (Character.isDigit(codigo.charAt(i))) {
                ultimoCodigo.append(codigo.charAt(i));
            }
        }
        return Integer.parseInt(ultimoCodigo.toString());
    }

    private Venta parseBufferVenta(CharBuffer registro) {

        String codigoFactura = registro.subSequence(0, LONGITUD_CODIGO_FACTURA).toString().trim();
        registro.position(LONGITUD_CODIGO_FACTURA);
        registro = registro.slice();

        String producto = registro.subSequence(0, LONGITUD_PRODUCTO).toString().trim();
        registro.position(LONGITUD_PRODUCTO);
        registro = registro.slice();

        int cantidad = Integer.parseInt(registro.subSequence(0, LONGITUD_CANTIDAD).toString().trim());
        registro.position(LONGITUD_CANTIDAD);
        registro = registro.slice();

        float precioVenta = Float.valueOf(registro.subSequence(0, LONGITUD_PRECIO_VENTA).toString().trim());
        registro.position(LONGITUD_PRECIO_VENTA);
        registro = registro.slice();

        LocalDate fecha = LocalDate.parse(registro.subSequence(0,LONGITUD_FECHA).toString().trim());
        registro.position(LONGITUD_FECHA);
        registro = registro.slice();


        Venta venta = new Venta(codigoFactura,producto,cantidad,precioVenta,fecha);

        return venta;
    }

    private String parseVentaString(Venta venta) {
        StringBuilder sb = new StringBuilder();
        sb.append(completarCampos(venta.getCodigoFactura(), LONGITUD_CODIGO_FACTURA))
                .append(completarCampos(venta.getProducto(), LONGITUD_PRODUCTO))
                .append(completarCampos(String.valueOf(venta.getCantidad()), LONGITUD_CANTIDAD))
                .append(completarCampos(String.valueOf(venta.getPrecioVenta()), LONGITUD_PRECIO_VENTA))
                .append(completarCampos(venta.getFecha().toString(),LONGITUD_FECHA));
        return sb.toString();
    }

    private String completarCampos(String dato, Integer longitudCampo) {
        return String.format("%1$" + longitudCampo + "s", dato);
    }
}
