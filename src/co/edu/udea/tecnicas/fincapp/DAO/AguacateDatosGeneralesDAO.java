package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;

import java.util.List;

public interface AguacateDatosGeneralesDAO {

    void registrarDatosGeneralesAguacate(DatosGeneralesCultivos datosGeneralesCultivos);

    List<DatosGeneralesCultivos> consultarDatosGeneralesAguacates();
    float consultarPrecioKg();
}
