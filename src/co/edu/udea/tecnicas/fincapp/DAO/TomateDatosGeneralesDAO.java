package co.edu.udea.tecnicas.fincapp.dao;

import co.edu.udea.tecnicas.fincapp.model.DatosGeneralesCultivos;

import java.util.List;

public interface TomateDatosGeneralesDAO {
    void registrarDatosGeneralesTomates(DatosGeneralesCultivos datosGeneralesCultivos);
    List<DatosGeneralesCultivos> consultarDatosGeneralesTomates();
    float consultarPrecioKg();
}
